//
// Created by dan on 28.08.2019.
//

#include "smhjsereader.h"

typedef struct {
    gpointer padding[1];
} SMHJSEReaderPrivate;

G_DEFINE_TYPE_WITH_CODE(SMHJSEReader, smh_jse_reader, JSE_TYPE_READER, G_ADD_PRIVATE(SMHJSEReader))

static void smh_jse_reader_class_init(SMHJSEReaderClass *class) {

}

static void smh_jse_reader_init(SMHJSEReader *self) {

}

void smh_jse_reader_state(SMHJSEReader *self, SMH__PBEDB *db) {
    (void)json_reader_read_member(JSON_READER(self), "partition");
    db->partition = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "step");
    db->step = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "active_client");
    const gchar *active_client = json_reader_get_string_value(JSON_READER(self));
    smh__pbedb__set_active_client(db, (gchar *)active_client);
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "session_id");
    const gchar *session_id = json_reader_get_string_value(JSON_READER(self));
    smh__pbedb__set_session_id(db, (gchar *)session_id);
    json_reader_end_member(JSON_READER(self));
}

void smh_jse_reader_room(SMHJSEReader *self, SMH__PBEDB *db, SMH__PBEDBRoom *room) {
    (void)json_reader_read_member(JSON_READER(self), "room_name");
    const gchar *name = json_reader_get_string_value(JSON_READER(self));
    smh__pbedbroom__set_name(room, (gchar *)name);
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "rmlt_rmlt_id");
    room->type = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));
}

void smh_jse_reader_groupdev(SMHJSEReader *self, SMH__PBEDB *db, SMH__PBEDBPeripheral *group) {
    (void)json_reader_read_member(JSON_READER(self), "room_room_id");
    group->room = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "group_x");
    group->x = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "group_y");
    group->y = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "group_z");
    group->z = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "grtp_grtp_id");
    group->group_type = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "grsn_grsn_id");
    group->group_sign = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "group_name");
    const gchar *name = json_reader_get_string_value(JSON_READER(self));
    smh__pbedbperipheral__set_name(group, (gchar *)name);
    json_reader_end_member(JSON_READER(self));
}

void smh_jse_reader_device(SMHJSEReader *self, SMH__PBEDB *db, SMH__PBEDBPeripheral *device) {
    (void)json_reader_read_member(JSON_READER(self), "dev_name");
    const gchar *name = json_reader_get_string_value(JSON_READER(self));
    smh__pbedbperipheral__set_name(device, (gchar *)name);
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "dev_x");
    device->x = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "dev_y");
    device->y = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "dev_z");
    device->z = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "dvsn_dvsn_id");
    device->device_sign = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "ctgr_ctgr_id");
    device->device_category = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "group_group_id");
    device->peripheral = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    g_autoptr(GArray) peripherals = smh__pbedb__ref_peripherals(db);
    GE_ARRAY_FILTER(peripherals, i, e, SMH__PBEDBPeripheral *, ((e->group_type == SMH__PBEDBPERIPHERAL__GROUP_TYPE__GROUP_TYPE_USER) && (e->id == device->peripheral)));
    if (peripherals->len > 0) {
        SMH__PBEDBPeripheral *group = g_array_index(peripherals, SMH__PBEDBPeripheral *, 0);

        if (group->n_clusters > 0) {
            GArray *group_clusters = smh__pbedbperipheral__get_clusters(group);

            for (gsize cluster_index = 0; cluster_index < group->n_clusters; cluster_index++) {
                SMH__PBEDBCluster *group_cluster = group->clusters[cluster_index];

                g_autoptr(GArray) device_clusters = smh__pbedbperipheral__ref_clusters(device);
                GE_ARRAY_FILTER(device_clusters, i, e, SMH__PBEDBCluster *, (e->id == group_cluster->id));
                if (device_clusters->len > 0) {
                    SMH__PBEDBCluster *device_cluster = g_array_index(device_clusters, SMH__PBEDBCluster *, 0);

                    GArray *group_attributes = smh__pbedbcluster__get_attributes(group_cluster);

                    for (gsize attribute_index = 0; attribute_index < group_cluster->n_attributes; attribute_index++) {
                        SMH__PBEDBAttribute *group_attribute = group_cluster->attributes[attribute_index];

                        g_autoptr(GArray) device_attributes = smh__pbedbcluster__ref_attributes(device_cluster);
                        GE_ARRAY_FILTER(device_attributes, i, e, SMH__PBEDBAttribute *, (e->key == group_attribute->key));
                        if (device_attributes->len > 0) {
                        } else {
                            GE_ARRAY_FILTER(group_attributes, i, e, SMH__PBEDBAttribute *, (e->key != group_attribute->key));
                        }
                    }

                    smh__pbedbcluster__set_attributes(group_cluster, group_attributes);
                } else {
                    GE_ARRAY_FILTER(group_clusters, i, e, SMH__PBEDBCluster *, (e->id != group_cluster->id));
                }
            }

            smh__pbedbperipheral__set_clusters(group, group_clusters);
        } else {
            GArray *group_clusters = smh__pbedbperipheral__get_clusters(group);

            for (gsize cluster_index = 0; cluster_index < device->n_clusters; cluster_index++) {
                SMH__PBEDBCluster *device_cluster = device->clusters[cluster_index];

                SMH__PBEDBCluster *group_cluster = smh__pbedbcluster__new();
                group_cluster->id = device_cluster->id;
                group_cluster->output = device_cluster->output;
                g_array_append_val(group_clusters, group_cluster);

                GArray *group_attributes = smh__pbedbcluster__get_attributes(group_cluster);

                for (gsize attribute_index = 0; attribute_index < device_cluster->n_attributes; attribute_index++) {
                    SMH__PBEDBAttribute *device_attribute = device_cluster->attributes[attribute_index];

                    SMH__PBEDBAttribute *group_attribute = smh__pbedbattribute__new();
                    group_attribute->key = device_attribute->key;
                    group_attribute->str_value = smh__pbedbattribute__get_str_value(device_attribute);
                    group_attribute->num_value = device_attribute->num_value;
                    g_array_append_val(group_attributes, group_attribute);
                }

                smh__pbedbcluster__set_attributes(group_cluster, group_attributes);
            }

            smh__pbedbperipheral__set_clusters(group, group_clusters);
        }
    }
}

void smh_jse_reader_opportunity(SMHJSEReader *self, SMH__PBEDB *db) {
    SMH__PBEDBCommand command = SMH__PBEDBCOMMAND__INIT;

    (void)json_reader_read_member(JSON_READER(self), "oppy_key");
    command.key = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "zcl_id");
    gint64 cluster_id = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    // Parameters

    (void)json_reader_read_member(JSON_READER(self), "params");

    command.n_parameters = json_reader_count_elements(JSON_READER(self));
    gint32 parameters[command.n_parameters];
    command.parameters = parameters;
    for (gint index = 0; index < command.n_parameters; index++) {
        (void)json_reader_read_element(JSON_READER(self), index);
        command.parameters[index] = json_reader_get_int_value(JSON_READER(self));
        json_reader_end_element(JSON_READER(self));
    }

    json_reader_end_member(JSON_READER(self));

    // Devices

    (void)json_reader_read_member(JSON_READER(self), "devices");

    gint count = json_reader_count_elements(JSON_READER(self));
    for (gint index = 0; index < count; index++) {
        (void)json_reader_read_element(JSON_READER(self), index);
        gint64 device_id = json_reader_get_int_value(JSON_READER(self));
        json_reader_end_element(JSON_READER(self));

        g_autoptr(GArray) peripherals = smh__pbedb__ref_peripherals(db);
        GE_ARRAY_FILTER(peripherals, i, e, SMH__PBEDBPeripheral *, (e->id == device_id));
        if (peripherals->len > 0) {
            SMH__PBEDBPeripheral *device = g_array_index(peripherals, SMH__PBEDBPeripheral *, 0);

            g_autoptr(GArray) clusters = smh__pbedbperipheral__ref_clusters(device);
            GE_ARRAY_FILTER(clusters, i, e, SMH__PBEDBCluster *, (e->id == cluster_id));
            if (clusters->len > 0) {
                SMH__PBEDBCluster *cluster = g_array_index(clusters, SMH__PBEDBCluster *, 0);

                SMHManager *manager = smh_manager_shared();
                smh_manager_peripheral_send_command(manager, db, device, cluster, &command);
            }
        }
    }

    json_reader_end_member(JSON_READER(self));

    // Groups

    (void)json_reader_read_member(JSON_READER(self), "groups");

    count = json_reader_count_elements(JSON_READER(self));
    for (gint index = 0; index < count; index++) {
        (void)json_reader_read_element(JSON_READER(self), index);
        gint64 group_id = json_reader_get_int_value(JSON_READER(self));
        json_reader_end_element(JSON_READER(self));

        g_autoptr(GArray) peripherals = smh__pbedb__ref_peripherals(db);
        GE_ARRAY_FILTER(peripherals, i, e, SMH__PBEDBPeripheral *, (e->id == group_id));
        if (peripherals->len > 0) {
            SMH__PBEDBPeripheral *group = g_array_index(peripherals, SMH__PBEDBPeripheral *, 0);

            g_autoptr(GArray) clusters = smh__pbedbperipheral__ref_clusters(group);
            GE_ARRAY_FILTER(clusters, i, e, SMH__PBEDBCluster *, (e->id == cluster_id));
            if (clusters->len > 0) {
                SMH__PBEDBCluster *cluster = g_array_index(clusters, SMH__PBEDBCluster *, 0);

                SMHManager *manager = smh_manager_shared();
                smh_manager_peripheral_send_command(manager, db, group, cluster, &command);
            }
        }
    }

    json_reader_end_member(JSON_READER(self));
}

void smh_jse_reader_script(SMHJSEReader *self, SMH__PBEDB *db, SMH__PBEDBAutomation *script) {
    (void)json_reader_read_member(JSON_READER(self), "scrt_name");
    const gchar *name = json_reader_get_string_value(JSON_READER(self));
    smh__pbedbautomation__set_name(script, (gchar *)name);
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "temp_temp_id");
    script->script_template = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "script_activate");
    script->on = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));
}

void smh_jse_reader_scheduler(SMHJSEReader *self, SMH__PBEDB *db, SMH__PBEDBScheduler *scheduler) {
    (void)json_reader_read_member(JSON_READER(self), "repeat");
    scheduler->repeat = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "start_time");
    const gchar *start_time = json_reader_get_string_value(JSON_READER(self));
    smh__pbedbscheduler__set_start_time(scheduler, (gchar *)start_time);
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "end_time");
    const gchar *end_time = json_reader_get_string_value(JSON_READER(self));
    smh__pbedbscheduler__set_end_time(scheduler, (gchar *)end_time);
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "scrt_scrt_id");
    scheduler->automation = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    // Weekdays

    GArray *weekdays = smh__pbedbscheduler__get_weekdays(scheduler);
    g_array_set_size(weekdays, 0);

    (void)json_reader_read_member(JSON_READER(self), "weekdays");

    gint count = json_reader_count_elements(JSON_READER(self));
    for (gint index = 0; index < count; index++) {
        (void)json_reader_read_element(JSON_READER(self), index);
        gint64 weekday = json_reader_get_int_value(JSON_READER(self));
        g_array_append_val(weekdays, weekday);
        json_reader_end_element(JSON_READER(self));
    }

    json_reader_end_member(JSON_READER(self));

    smh__pbedbscheduler__set_weekdays(scheduler, weekdays);
}

void smh_jse_reader_action(SMHJSEReader *self, SMH__PBEDB *db, SMH__PBEDBAction *action) {
    (void)json_reader_read_member(JSON_READER(self), "mmtp_mmtp_id");
    action->moment = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "zcl_zcl_id");
    action->cluster = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "oppy_key");
    action->command = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "scrt_scrt_id");
    action->automation = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "act_num");
    action->number = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    // Parameters

    GArray *parameters = smh__pbedbaction__get_parameters(action);
    g_array_set_size(parameters, 0);

    (void)json_reader_read_member(JSON_READER(self), "params");

    gint count = json_reader_count_elements(JSON_READER(self));
    for (gint index = 0; index < count; index++) {
        (void)json_reader_read_element(JSON_READER(self), index);
        gint64 parameter = json_reader_get_int_value(JSON_READER(self));
        g_array_append_val(parameters, parameter);
        json_reader_end_element(JSON_READER(self));
    }

    json_reader_end_member(JSON_READER(self));

    smh__pbedbaction__set_parameters(action, parameters);

    // Devices

    GArray *devices = smh__pbedbaction__get_devices(action);
    g_array_set_size(devices, 0);

    (void)json_reader_read_member(JSON_READER(self), "devices");

    count = json_reader_count_elements(JSON_READER(self));
    for (gint index = 0; index < count; index++) {
        (void)json_reader_read_element(JSON_READER(self), index);
        gint64 device = json_reader_get_int_value(JSON_READER(self));
        g_array_append_val(devices, device);
        json_reader_end_element(JSON_READER(self));
    }

    json_reader_end_member(JSON_READER(self));

    smh__pbedbaction__set_devices(action, devices);

    // Groups

    GArray *groups = smh__pbedbaction__get_groups(action);
    g_array_set_size(groups, 0);

    (void)json_reader_read_member(JSON_READER(self), "groups");

    count = json_reader_count_elements(JSON_READER(self));
    for (gint index = 0; index < count; index++) {
        (void)json_reader_read_element(JSON_READER(self), index);
        gint64 group = json_reader_get_int_value(JSON_READER(self));
        g_array_append_val(groups, group);
        json_reader_end_element(JSON_READER(self));
    }

    json_reader_end_member(JSON_READER(self));

    smh__pbedbaction__set_groups(action, groups);
}

void smh_jse_reader_condition_sensor(SMHJSEReader *self, SMH__PBEDB *db, SMH__PBEDBCondition *condition) {
    (void)json_reader_read_member(JSON_READER(self), "cnsn_zcl_id");
    condition->cluster = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "cnsn_attr_key");
    condition->attribute = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "cnsn_type");
    condition->relation = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "sensor_value");
    condition->value = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "dev_dev_id");
    condition->peripheral = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "scrt_scrt_id");
    condition->automation = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));
}

void smh_jse_reader_mode(SMHJSEReader *self, SMH__PBEDB *db, SMH__PBEDBAutomation *mode) {
    (void)json_reader_read_member(JSON_READER(self), "mode_name");
    const gchar *name = json_reader_get_string_value(JSON_READER(self));
    smh__pbedbautomation__set_name(mode, (gchar *)name);
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "on");
    mode->on = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "mode_type");
    mode->mode_type = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "mode_sign");
    mode->mode_sign = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    // Schedulers

    GArray *schedulers = smh__pbedb__get_schedulers(db);
    GE_ARRAY_FILTER(schedulers, i, e, SMH__PBEDBScheduler *, (e->automation != mode->id));

    (void)json_reader_read_member(JSON_READER(self), "schedulers");

    gint count = json_reader_count_elements(JSON_READER(self));
    for (gint index = 0; index < count; index++) {
        (void)json_reader_read_element(JSON_READER(self), index);
        SMH__PBEDBScheduler *scheduler = smh__pbedbscheduler__new();
        scheduler->type = SMH__PBEDBAUTOMATION__TYPE__TYPE_MODE;
        scheduler->id = ++db->scheduler_id;
        smh_jse_reader_scheduler(self, db, scheduler);
        scheduler->automation = mode->id;
        g_array_append_val(schedulers, scheduler);
        json_reader_end_element(JSON_READER(self));
    }

    json_reader_end_member(JSON_READER(self));

    smh__pbedb__set_schedulers(db, schedulers);

    // Conditions

    GArray *conditions = smh__pbedb__get_conditions(db);
    GE_ARRAY_FILTER(conditions, i, e, SMH__PBEDBCondition *, (e->automation != mode->id));

    (void)json_reader_read_member(JSON_READER(self), "activate_condition_sensors");

    count = json_reader_count_elements(JSON_READER(self));
    for (gint index = 0; index < count; index++) {
        (void)json_reader_read_element(JSON_READER(self), index);
        SMH__PBEDBCondition *condition = smh__pbedbcondition__new();
        condition->type = SMH__PBEDBAUTOMATION__TYPE__TYPE_MODE;
        condition->id = ++db->condition_id;
        condition->moment = SMH__PBEDBACTION__MOMENT__MOMENT_BEGIN;
        smh_jse_reader_condition_sensor(self, db, condition);
        condition->automation = mode->id;
        g_array_append_val(conditions, condition);
        json_reader_end_element(JSON_READER(self));
    }

    json_reader_end_member(JSON_READER(self));

    //

    (void)json_reader_read_member(JSON_READER(self), "deactivate_condition_sensors");

    count = json_reader_count_elements(JSON_READER(self));
    for (gint index = 0; index < count; index++) {
        (void)json_reader_read_element(JSON_READER(self), index);
        SMH__PBEDBCondition *condition = smh__pbedbcondition__new();
        condition->type = SMH__PBEDBAUTOMATION__TYPE__TYPE_MODE;
        condition->id = ++db->condition_id;
        condition->moment = SMH__PBEDBACTION__MOMENT__MOMENT_END;
        smh_jse_reader_condition_sensor(self, db, condition);
        condition->automation = mode->id;
        g_array_append_val(conditions, condition);
        json_reader_end_element(JSON_READER(self));
    }

    json_reader_end_member(JSON_READER(self));

    smh__pbedb__set_conditions(db, conditions);

    // Actions

    GArray *actions = smh__pbedb__get_actions(db);
    GE_ARRAY_FILTER(actions, i, e, SMH__PBEDBAction *, (e->automation != mode->id));

    (void)json_reader_read_member(JSON_READER(self), "actions");

    count = json_reader_count_elements(JSON_READER(self));
    for (gint index = 0; index < count; index++) {
        (void)json_reader_read_element(JSON_READER(self), index);
        SMH__PBEDBAction *action = smh__pbedbaction__new();
        action->type = SMH__PBEDBAUTOMATION__TYPE__TYPE_MODE;
        action->id = ++db->action_id;
        smh_jse_reader_action(self, db, action);
        action->automation = mode->id;
        g_array_append_val(actions, action);
        json_reader_end_element(JSON_READER(self));
    }

    json_reader_end_member(JSON_READER(self));

    smh__pbedb__set_actions(db, actions);
}

void smh_jse_reader_configure_button(SMHJSEReader *self, SMH__PBEDB *db) {
    (void)json_reader_read_member(JSON_READER(self), "dev_id");
    guint32 id = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    g_autoptr(GArray) peripherals = smh__pbedb__ref_peripherals(db);
    GE_ARRAY_FILTER(peripherals, i, e, SMH__PBEDBPeripheral *, (e->id == id));
    if (peripherals->len > 0) {
        SMH__PBEDBPeripheral *device = g_array_index(peripherals, SMH__PBEDBPeripheral *, 0);

        GArray *bindings = smh__pbedbperipheral__get_bindings(device);
        g_array_set_size(bindings, 0);

        (void)json_reader_read_member(JSON_READER(self), "bindings");

        gint count = json_reader_count_elements(JSON_READER(self));
        for (gint index = 0; index < count; index++) {
            (void)json_reader_read_element(JSON_READER(self), index);

            SMH__PBEDBBinding *binding = smh__pbedbbinding__new();

            (void)json_reader_read_member(JSON_READER(self), "key_id");
            binding->key_id = json_reader_get_int_value(JSON_READER(self));
            json_reader_end_member(JSON_READER(self));

            (void)json_reader_read_member(JSON_READER(self), "object_type");
            binding->object_type = json_reader_get_int_value(JSON_READER(self));
            json_reader_end_member(JSON_READER(self));

            (void)json_reader_read_member(JSON_READER(self), "object_id");
            binding->object_id = json_reader_get_int_value(JSON_READER(self));
            json_reader_end_member(JSON_READER(self));

            (void)json_reader_read_member(JSON_READER(self), "command_id");
            binding->command_id = json_reader_get_int_value(JSON_READER(self));
            json_reader_end_member(JSON_READER(self));

            g_array_append_val(bindings, binding);

            json_reader_end_element(JSON_READER(self));
        }

        json_reader_end_member(JSON_READER(self));

        smh__pbedbperipheral__set_bindings(device, bindings);
    }
}

void smh_jse_reader_mobile_registration(SMHJSEReader *self, SMH__PBEDB *db, SMH__PBEDBSubscriber *subscriber) {
    (void)json_reader_read_member(JSON_READER(self), "mobile_name");
    const gchar *name = json_reader_get_string_value(JSON_READER(self));
    smh__pbedbsubscriber__set_name(subscriber, (gchar *)name);
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "os");
    const gchar *os = json_reader_get_string_value(JSON_READER(self));
    smh__pbedbsubscriber__set_os(subscriber, (gchar *)os);
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "hw_id");
    const gchar *hw_id = json_reader_get_string_value(JSON_READER(self));
    smh__pbedbsubscriber__set_hw_id(subscriber, (gchar *)hw_id);
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "registration_token");
    const gchar *registration_token = json_reader_get_string_value(JSON_READER(self));
    smh__pbedbsubscriber__set_registration_token(subscriber, (gchar *)registration_token);
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "STBdisconnect");
    subscriber->stb_disconnect = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));

    (void)json_reader_read_member(JSON_READER(self), "duration");
    subscriber->duration = json_reader_get_int_value(JSON_READER(self));
    json_reader_end_member(JSON_READER(self));
}
