//
// Created by dan on 28.08.2019.
//

#ifndef LIBRARY_SMARTHOME_SMHJSEPARSER_H
#define LIBRARY_SMARTHOME_SMHJSEPARSER_H

#include "smhmain.h"

G_BEGIN_DECLS

#define SMH_TYPE_JSE_PARSER smh_jse_parser_get_type()

G_DECLARE_DERIVABLE_TYPE(SMHJSEParser, smh_jse_parser, SMH, JSE_PARSER, JSEParser)

struct _SMHJSEParserClass {
    JSEParserClass super;
};

G_END_DECLS

#endif //LIBRARY_SMARTHOME_SMHJSEPARSER_H
