//
// Created by dan on 05.09.2019.
//

#include "smhmanager.h"

typedef struct {
    gpointer padding[1];
} SMHManagerPrivate;

enum {
    SMH_MANAGER_SIGNAL_PERIPHERAL_VALUE_SET = 1,
    SMH_MANAGER_SIGNAL_AUTOMATION_ON_SET,
    _SMH_MANAGER_SIGNAL_COUNT
};

static guint smh_manager_signals[_SMH_MANAGER_SIGNAL_COUNT] = {0};

static void _smh_manager_peripheral_value_set(SMHManager *self, SMH__PBEDB *db, SMH__PBEDBPeripheral *peripheral, SMH__PBEDBCluster *cluster, SMH__PBEDBAttribute *attribute);
static void _smh_manager_automation_on_set(SMHManager *self, SMH__PBEDB *db, SMH__PBEDBAutomation *automation);

G_DEFINE_TYPE_WITH_CODE(SMHManager, smh_manager, GE_TYPE_OBJECT, G_ADD_PRIVATE(SMHManager))

GE_POINTER_SHARED_C(SMHManager, smh_manager)

static void smh_manager_class_init(SMHManagerClass *class) {
    class->peripheral_value_set = _smh_manager_peripheral_value_set;
    class->automation_on_set = _smh_manager_automation_on_set;

    smh_manager_signals[SMH_MANAGER_SIGNAL_PERIPHERAL_VALUE_SET] = g_signal_new("peripheral-value-set", SMH_TYPE_MANAGER, G_SIGNAL_RUN_LAST, G_STRUCT_OFFSET(SMHManagerClass, peripheral_value_set), NULL, NULL, NULL, G_TYPE_NONE, 4, G_TYPE_POINTER, G_TYPE_POINTER, G_TYPE_POINTER, G_TYPE_POINTER);
    smh_manager_signals[SMH_MANAGER_SIGNAL_AUTOMATION_ON_SET] = g_signal_new("automation-on-set", SMH_TYPE_MANAGER, G_SIGNAL_RUN_LAST, G_STRUCT_OFFSET(SMHManagerClass, automation_on_set), NULL, NULL, NULL, G_TYPE_NONE, 2, G_TYPE_POINTER, G_TYPE_POINTER);
}

static void smh_manager_init(SMHManager *self) {

}

void smh_manager_peripheral_value_set(SMHManager *self, SMH__PBEDB *db, SMH__PBEDBPeripheral *peripheral, SMH__PBEDBCluster *cluster, SMH__PBEDBAttribute *attribute) {
    g_signal_emit(self, smh_manager_signals[SMH_MANAGER_SIGNAL_PERIPHERAL_VALUE_SET], 0, db, peripheral, cluster, attribute);
}

static void _smh_manager_peripheral_value_set(SMHManager *self, SMH__PBEDB *db, SMH__PBEDBPeripheral *peripheral, SMH__PBEDBCluster *cluster, SMH__PBEDBAttribute *attribute) {
    g_print("peripheral-value-set\n");
}

void smh_manager_automation_on_set(SMHManager *self, SMH__PBEDB *db, SMH__PBEDBAutomation *automation) {
    g_signal_emit(self, smh_manager_signals[SMH_MANAGER_SIGNAL_AUTOMATION_ON_SET], 0, db, automation);
}

static void _smh_manager_automation_on_set(SMHManager *self, SMH__PBEDB *db, SMH__PBEDBAutomation *automation) {
    g_print("automation-on-set\n");
}

void smh_manager_peripheral_send_command(SMHManager *self, SMH__PBEDB *db, SMH__PBEDBPeripheral *peripheral, SMH__PBEDBCluster *cluster, SMH__PBEDBCommand *command) {
    if (cluster->id == SMH__PBEDBCLUSTER__ID__ID_ON_OFF) {
        if (command->key == SMH__PBEDBCOMMAND__KEY__KEY_ON_OFF_OFF) {
            g_autoptr(GArray) attributes = smh__pbedbcluster__ref_attributes(cluster);
            GE_ARRAY_FILTER(attributes, i, e, SMH__PBEDBAttribute *, (e->key == SMH__PBEDBATTRIBUTE__KEY__KEY_ON_OFF_VALUE));
            if (attributes->len > 0) {
                SMH__PBEDBAttribute *attribute = g_array_index(attributes, SMH__PBEDBAttribute *, 0);
                smh__pbedbattribute__set_str_value(attribute, "0");
                smh_manager_peripheral_set_value(self, db, peripheral, cluster, attribute);
            }
        } else if (command->key == SMH__PBEDBCOMMAND__KEY__KEY_ON_OFF_ON) {
            g_autoptr(GArray) attributes = smh__pbedbcluster__ref_attributes(cluster);
            GE_ARRAY_FILTER(attributes, i, e, SMH__PBEDBAttribute *, (e->key == SMH__PBEDBATTRIBUTE__KEY__KEY_ON_OFF_VALUE));
            if (attributes->len > 0) {
                SMH__PBEDBAttribute *attribute = g_array_index(attributes, SMH__PBEDBAttribute *, 0);
                smh__pbedbattribute__set_str_value(attribute, "1");
                smh_manager_peripheral_set_value(self, db, peripheral, cluster, attribute);
            }
        } else if (command->key == SMH__PBEDBCOMMAND__KEY__KEY_ON_OFF_TOGGLE) {
            g_autoptr(GArray) attributes = smh__pbedbcluster__ref_attributes(cluster);
            GE_ARRAY_FILTER(attributes, i, e, SMH__PBEDBAttribute *, (e->key == SMH__PBEDBATTRIBUTE__KEY__KEY_ON_OFF_VALUE));
            if (attributes->len > 0) {
                SMH__PBEDBAttribute *attribute = g_array_index(attributes, SMH__PBEDBAttribute *, 0);

                if (g_str_equal(attribute->str_value, "0")) {
                    smh__pbedbattribute__set_str_value(attribute, "1");
                } else {
                    smh__pbedbattribute__set_str_value(attribute, "0");
                }

                smh_manager_peripheral_set_value(self, db, peripheral, cluster, attribute);
            }
        }
    } else if (cluster->id == SMH__PBEDBCLUSTER__ID__ID_LEVEL) {
        if (command->key == SMH__PBEDBCOMMAND__KEY__KEY_LEVEL_VALUE) {
            g_autoptr(GArray) attributes = smh__pbedbcluster__ref_attributes(cluster);
            GE_ARRAY_FILTER(attributes, i, e, SMH__PBEDBAttribute *, (e->key == SMH__PBEDBATTRIBUTE__KEY__KEY_LEVEL_VALUE));
            if (attributes->len > 0) {
                SMH__PBEDBAttribute *attribute = g_array_index(attributes, SMH__PBEDBAttribute *, 0);
                g_autofree gchar *value = g_strdup_printf("%i", command->parameters[0]);
                smh__pbedbattribute__set_str_value(attribute, value);
                smh_manager_peripheral_set_value(self, db, peripheral, cluster, attribute);
            }
        }
    } else if (cluster->id == SMH__PBEDBCLUSTER__ID__ID_COLOR) {
        if (command->key == SMH__PBEDBCOMMAND__KEY__KEY_COLOR_SATURATION) {
            g_autoptr(GArray) attributes_saturation = smh__pbedbcluster__ref_attributes(cluster);
            GE_ARRAY_FILTER(attributes_saturation, i, e, SMH__PBEDBAttribute *, (e->key == SMH__PBEDBATTRIBUTE__KEY__KEY_COLOR_SATURATION));
            if (attributes_saturation->len > 0) {
                SMH__PBEDBAttribute *attribute = g_array_index(attributes_saturation, SMH__PBEDBAttribute *, 0);
                g_autofree gchar *value = g_strdup_printf("%i", command->parameters[0]);
                smh__pbedbattribute__set_str_value(attribute, value);
                smh_manager_peripheral_set_value(self, db, peripheral, cluster, attribute);
            }

            g_autoptr(GArray) attributes_mode = smh__pbedbcluster__ref_attributes(cluster);
            GE_ARRAY_FILTER(attributes_mode, i, e, SMH__PBEDBAttribute *, (e->key == SMH__PBEDBATTRIBUTE__KEY__KEY_COLOR_MODE));
            if (attributes_mode->len > 0) {
                SMH__PBEDBAttribute *attribute = g_array_index(attributes_mode, SMH__PBEDBAttribute *, 0);
                g_autofree gchar *value = g_strdup_printf("%i", SMH__PBEDBATTRIBUTE__COLOR_MODE__COLOR_MODE_HUE_SATURATION);
                smh__pbedbattribute__set_str_value(attribute, value);
                smh_manager_peripheral_set_value(self, db, peripheral, cluster, attribute);
            }
        } else if (command->key == SMH__PBEDBCOMMAND__KEY__KEY_COLOR_HUE_SATURATION) {
            g_autoptr(GArray) attributes_hue = smh__pbedbcluster__ref_attributes(cluster);
            GE_ARRAY_FILTER(attributes_hue, i, e, SMH__PBEDBAttribute *, (e->key == SMH__PBEDBATTRIBUTE__KEY__KEY_COLOR_HUE));
            if (attributes_hue->len > 0) {
                SMH__PBEDBAttribute *attribute = g_array_index(attributes_hue, SMH__PBEDBAttribute *, 0);
                g_autofree gchar *value = g_strdup_printf("%i", command->parameters[0]);
                smh__pbedbattribute__set_str_value(attribute, value);
                smh_manager_peripheral_set_value(self, db, peripheral, cluster, attribute);
            }

            g_autoptr(GArray) attributes_saturation = smh__pbedbcluster__ref_attributes(cluster);
            GE_ARRAY_FILTER(attributes_saturation, i, e, SMH__PBEDBAttribute *, (e->key == SMH__PBEDBATTRIBUTE__KEY__KEY_COLOR_SATURATION));
            if (attributes_saturation->len > 0) {
                SMH__PBEDBAttribute *attribute = g_array_index(attributes_saturation, SMH__PBEDBAttribute *, 0);
                g_autofree gchar *value = g_strdup_printf("%i", command->parameters[1]);
                smh__pbedbattribute__set_str_value(attribute, value);
                smh_manager_peripheral_set_value(self, db, peripheral, cluster, attribute);
            }

            g_autoptr(GArray) attributes_mode = smh__pbedbcluster__ref_attributes(cluster);
            GE_ARRAY_FILTER(attributes_mode, i, e, SMH__PBEDBAttribute *, (e->key == SMH__PBEDBATTRIBUTE__KEY__KEY_COLOR_MODE));
            if (attributes_mode->len > 0) {
                SMH__PBEDBAttribute *attribute = g_array_index(attributes_mode, SMH__PBEDBAttribute *, 0);
                g_autofree gchar *value = g_strdup_printf("%i", SMH__PBEDBATTRIBUTE__COLOR_MODE__COLOR_MODE_HUE_SATURATION);
                smh__pbedbattribute__set_str_value(attribute, value);
                smh_manager_peripheral_set_value(self, db, peripheral, cluster, attribute);
            }
        } else if (command->key == SMH__PBEDBCOMMAND__KEY__KEY_COLOR_TEMPERATURE) {
            g_autoptr(GArray) attributes_temperature = smh__pbedbcluster__ref_attributes(cluster);
            GE_ARRAY_FILTER(attributes_temperature, i, e, SMH__PBEDBAttribute *, (e->key == SMH__PBEDBATTRIBUTE__KEY__KEY_COLOR_TEMPERATURE));
            if (attributes_temperature->len > 0) {
                SMH__PBEDBAttribute *attribute = g_array_index(attributes_temperature, SMH__PBEDBAttribute *, 0);
                g_autofree gchar *value = g_strdup_printf("%i", command->parameters[0]);
                smh__pbedbattribute__set_str_value(attribute, value);
                smh_manager_peripheral_set_value(self, db, peripheral, cluster, attribute);
            }

            g_autoptr(GArray) attributes_mode = smh__pbedbcluster__ref_attributes(cluster);
            GE_ARRAY_FILTER(attributes_mode, i, e, SMH__PBEDBAttribute *, (e->key == SMH__PBEDBATTRIBUTE__KEY__KEY_COLOR_MODE));
            if (attributes_mode->len > 0) {
                SMH__PBEDBAttribute *attribute = g_array_index(attributes_mode, SMH__PBEDBAttribute *, 0);
                g_autofree gchar *value = g_strdup_printf("%i", SMH__PBEDBATTRIBUTE__COLOR_MODE__COLOR_MODE_TEMPERATURE);
                smh__pbedbattribute__set_str_value(attribute, value);
                smh_manager_peripheral_set_value(self, db, peripheral, cluster, attribute);
            }
        }
    } else if (cluster->id == SMH__PBEDBCLUSTER__ID__ID_SIREN) {
        if (command->key == SMH__PBEDBCOMMAND__KEY__KEY_SIREN_WARN) {
            gint32 alarm = (command->parameters[0] & SMH__PBEDBCOMMAND__SIREN_WARNING__SIREN_WARNING_ALARM);

            g_autoptr(GArray) clusters = smh__pbedbperipheral__ref_clusters(peripheral);
            GE_ARRAY_FILTER(clusters, i, e, SMH__PBEDBCluster *, (e->id == SMH__PBEDBCLUSTER__ID__ID_IAS));
            if (clusters->len > 0) {
                cluster = g_array_index(clusters, SMH__PBEDBCluster *, 0);

                g_autoptr(GArray) attributes = smh__pbedbcluster__ref_attributes(cluster);
                GE_ARRAY_FILTER(attributes, i, e, SMH__PBEDBAttribute *, (e->key == SMH__PBEDBATTRIBUTE__KEY__KEY_IAS_STATUS));
                if (attributes->len > 0) {
                    SMH__PBEDBAttribute *attribute = g_array_index(attributes, SMH__PBEDBAttribute *, 0);
                    g_autofree gchar *value = g_strdup_printf("%i", (alarm != 0));
                    smh__pbedbattribute__set_str_value(attribute, value);
                    smh_manager_peripheral_set_value(self, db, peripheral, cluster, attribute);
                }
            }
        }
    }

    if (peripheral->type == SMH__PBEDBPERIPHERAL__TYPE__TYPE_GROUP) {
        g_autoptr(GArray) peripherals = smh__pbedb__ref_peripherals(db);
        GE_ARRAY_FILTER(peripherals, i, e, SMH__PBEDBPeripheral *, (e->peripheral == peripheral->id));
        for (guint index = 0; index < peripherals->len; index++) {
            SMH__PBEDBPeripheral *device = g_array_index(peripherals, SMH__PBEDBPeripheral *, index);
            smh_manager_peripheral_send_command(self, db, device, cluster, command);
        }
    }
}

void smh_manager_peripheral_set_value(SMHManager *self, SMH__PBEDB *db, SMH__PBEDBPeripheral *peripheral, SMH__PBEDBCluster *cluster, SMH__PBEDBAttribute *attribute) {
    smh_manager_peripheral_value_set(self, db, peripheral, cluster, attribute);
}

void smh_manager_automation_set_on(SMHManager *self, SMH__PBEDB *db, SMH__PBEDBAutomation *automation) {
    smh_manager_automation_on_set(self, db, automation);

    g_autoptr(GArray) schedulers = smh__pbedb__ref_schedulers(db);
    GE_ARRAY_FILTER(schedulers, i, e, SMH__PBEDBScheduler *, (e->automation == automation->id));
    if (schedulers->len > 0) {
        return;
    }

    if (automation->type == SMH__PBEDBAUTOMATION__TYPE__TYPE_SCRIPT) {
        g_autoptr(GArray) conditions = smh__pbedb__ref_conditions(db);
        GE_ARRAY_FILTER(conditions, i, e, SMH__PBEDBCondition *, (e->automation == automation->id));
        if (conditions->len > 0) {
            return;
        }

        g_autoptr(GArray) actions = smh__pbedb__ref_actions(db);
        GE_ARRAY_FILTER(actions, i, e, SMH__PBEDBAction *, ((e->automation == automation->id) && (e->moment == SMH__PBEDBACTION__MOMENT__MOMENT_BEGIN) && ((e->n_devices > 0) || (e->n_groups > 0))));
        GE_ARRAY_SORT(actions, i, a, b, SMH__PBEDBAction *, (a->number > b->number));
        for (guint action_index = 0; action_index < actions->len; action_index++) {
            SMH__PBEDBAction *action = g_array_index(actions, SMH__PBEDBAction *, action_index);

            g_autoptr(GArray) ids = smh__pbedbaction__ref_devices(action);
            g_array_append_vals(ids, action->groups, action->n_groups);
            for (guint peripheral_index = 0; peripheral_index < ids->len; peripheral_index++) {
                guint32 id = g_array_index(ids, guint32, peripheral_index);

                g_autoptr(GArray) peripherals = smh__pbedb__ref_peripherals(db);
                GE_ARRAY_FILTER(peripherals, i, e, SMH__PBEDBPeripheral *, (e->id == id));
                if (peripherals->len > 0) {
                    SMH__PBEDBPeripheral *peripheral = g_array_index(peripherals, SMH__PBEDBPeripheral *, 0);

                    g_autoptr(GArray) clusters = smh__pbedbperipheral__ref_clusters(peripheral);
                    GE_ARRAY_FILTER(clusters, i, e, SMH__PBEDBCluster *, (e->id == action->cluster));
                    if (clusters->len > 0) {
                        SMH__PBEDBCluster *cluster = g_array_index(clusters, SMH__PBEDBCluster *, 0);

                        SMH__PBEDBCommand command = SMH__PBEDBCOMMAND__INIT;
                        command.key = action->command;
                        command.n_parameters = action->n_parameters;
                        gint32 parameters[command.n_parameters];
                        command.parameters = parameters;
                        memcpy(command.parameters, action->parameters, (command.n_parameters * sizeof(gint32)));

                        smh_manager_peripheral_send_command(self, db, peripheral, cluster, &command);
                    }
                }
            }
        }
    } else {
        SMH__PBEDBAction__Moment moment = (automation->on == TRUE) ? SMH__PBEDBACTION__MOMENT__MOMENT_BEGIN : SMH__PBEDBACTION__MOMENT__MOMENT_END;

        g_autoptr(GArray) conditions = smh__pbedb__ref_conditions(db);
        GE_ARRAY_FILTER(conditions, i, e, SMH__PBEDBCondition *, ((e->automation == automation->id) && (e->moment == moment)));
        if (conditions->len > 0) {
            return;
        }

        g_autoptr(GArray) actions = smh__pbedb__ref_actions(db);
        GE_ARRAY_FILTER(actions, i, e, SMH__PBEDBAction *, ((e->automation == automation->id) && (e->moment == moment) && (e->cluster == SMH__PBEDBCLUSTER__ID__ID_SCRIPT) && (e->command == SMH__PBEDBCOMMAND__KEY__KEY_SCRIPT_ON_UNCONDITIONAL) && (e->n_devices > 0)));
        GE_ARRAY_SORT(actions, i, a, b, SMH__PBEDBAction *, (a->number > b->number));
        for (guint action_index = 0; action_index < actions->len; action_index++) {
            SMH__PBEDBAction *action = g_array_index(actions, SMH__PBEDBAction *, action_index);

            for (gsize script_index = 0; script_index < action->n_devices; script_index++) {
                guint32 id = action->devices[script_index];

                g_autoptr(GArray) automations = smh__pbedb__ref_automations(db);
                GE_ARRAY_FILTER(automations, i, e, SMH__PBEDBAutomation *, (e->id == id));
                if (automations->len > 0) {
                    SMH__PBEDBAutomation *script = g_array_index(automations, SMH__PBEDBAutomation *, 0);
                    script->on = TRUE;

                    smh_manager_automation_set_on(self, db, script);
                }
            }
        }
    }
}

SMHManager *_smh_manager_shared(void) {
    SMHManager *self = g_object_new(SMH_TYPE_MANAGER, NULL);
    return self;
}
