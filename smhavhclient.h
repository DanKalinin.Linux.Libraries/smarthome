//
// Created by dan on 11.08.2019.
//

#ifndef LIBRARY_SMARTHOME_SMHAVHCLIENT_H
#define LIBRARY_SMARTHOME_SMHAVHCLIENT_H

#include "smhmain.h"

G_BEGIN_DECLS

#define SMH_TYPE_AVH_CLIENT smh_avh_client_get_type()

G_DECLARE_DERIVABLE_TYPE(SMHAVHClient, smh_avh_client, SMH, AVH_CLIENT, AVHClient)

struct _SMHAVHClientClass {
    AVHClientClass super;
};

GE_POINTER_SHARED(SMHAVHClient, smh_avh_client)

G_END_DECLS

#endif //LIBRARY_SMARTHOME_SMHAVHCLIENT_H
