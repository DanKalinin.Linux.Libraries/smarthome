#!/bin/bash

echo "***** Cloning Libraries *****"

mkdir Libraries
cd Libraries

git clone https://gitlab.com/DanKalinin.Linux.Libraries/glib-ext.git
git clone https://gitlab.com/DanKalinin.Linux.Libraries/gtk-ext.git
git clone https://gitlab.com/DanKalinin.Linux.Libraries/avahi-ext.git
git clone https://gitlab.com/DanKalinin.Linux.Libraries/protobuf-ext.git
git clone https://gitlab.com/DanKalinin.Linux.Libraries/soup-ext.git
git clone https://gitlab.com/DanKalinin.Linux.Libraries/json-ext.git
git clone https://gitlab.com/DanKalinin.Linux.Libraries/smarthome.git

echo "***** Cloning Executables *****"

cd ..
mkdir Executables
cd Executables

git clone https://gitlab.com/DanKalinin.Linux.Executables/smarthome.git
