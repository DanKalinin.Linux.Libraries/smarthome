//
// Created by dan on 28.08.2019.
//

#ifndef LIBRARY_SMARTHOME_SMHJSEREADER_H
#define LIBRARY_SMARTHOME_SMHJSEREADER_H

#include "smhmain.h"
#include "smhpbedb.h"
#include "smhmanager.h"

G_BEGIN_DECLS

#define SMH_TYPE_JSE_READER smh_jse_reader_get_type()

G_DECLARE_DERIVABLE_TYPE(SMHJSEReader, smh_jse_reader, SMH, JSE_READER, JSEReader)

struct _SMHJSEReaderClass {
    JSEReaderClass super;
};

void smh_jse_reader_state(SMHJSEReader *self, SMH__PBEDB *db);
void smh_jse_reader_room(SMHJSEReader *self, SMH__PBEDB *db, SMH__PBEDBRoom *room);
void smh_jse_reader_groupdev(SMHJSEReader *self, SMH__PBEDB *db, SMH__PBEDBPeripheral *group);
void smh_jse_reader_device(SMHJSEReader *self, SMH__PBEDB *db, SMH__PBEDBPeripheral *device);
void smh_jse_reader_opportunity(SMHJSEReader *self, SMH__PBEDB *db);
void smh_jse_reader_script(SMHJSEReader *self, SMH__PBEDB *db, SMH__PBEDBAutomation *script);
void smh_jse_reader_scheduler(SMHJSEReader *self, SMH__PBEDB *db, SMH__PBEDBScheduler *scheduler);
void smh_jse_reader_action(SMHJSEReader *self, SMH__PBEDB *db, SMH__PBEDBAction *action);
void smh_jse_reader_condition_sensor(SMHJSEReader *self, SMH__PBEDB *db, SMH__PBEDBCondition *condition);
void smh_jse_reader_mode(SMHJSEReader *self, SMH__PBEDB *db, SMH__PBEDBAutomation *mode);
void smh_jse_reader_configure_button(SMHJSEReader *self, SMH__PBEDB *db);
void smh_jse_reader_mobile_registration(SMHJSEReader *self, SMH__PBEDB *db, SMH__PBEDBSubscriber *subscriber);

G_END_DECLS

#endif //LIBRARY_SMARTHOME_SMHJSEREADER_H
