//
// Created by dan on 17.07.19.
//

#ifndef LIBRARY_SMARTHOME_SMHMAIN_H
#define LIBRARY_SMARTHOME_SMHMAIN_H

#include <glib-ext/glib-ext.h>
#include <gtk-ext/gtk-ext.h>
#include <avahi-ext/avahi-ext.h>
#include <protobuf-ext/protobuf-ext.h>
#include <soup-ext/soup-ext.h>
#include <json-ext/json-ext.h>

#endif //LIBRARY_SMARTHOME_SMHMAIN_H
