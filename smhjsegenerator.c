//
// Created by dan on 23.08.2019.
//

#include "smhjsegenerator.h"

typedef struct {
    gpointer padding[1];
} SMHJSEGeneratorPrivate;

G_DEFINE_TYPE_WITH_CODE(SMHJSEGenerator, smh_jse_generator, JSE_TYPE_GENERATOR, G_ADD_PRIVATE(SMHJSEGenerator))

static void smh_jse_generator_class_init(SMHJSEGeneratorClass *class) {

}

static void smh_jse_generator_init(SMHJSEGenerator *self) {

}
