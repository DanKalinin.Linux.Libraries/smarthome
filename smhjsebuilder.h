//
// Created by dan on 23.08.2019.
//

#ifndef LIBRARY_SMARTHOME_SMHJSEBUILDER_H
#define LIBRARY_SMARTHOME_SMHJSEBUILDER_H

#include "smhmain.h"
#include "smhpbedb.h"

G_BEGIN_DECLS

#define SMH_TYPE_JSE_BUILDER smh_jse_builder_get_type()

G_DECLARE_DERIVABLE_TYPE(SMHJSEBuilder, smh_jse_builder, SMH, JSE_BUILDER, JSEBuilder)

struct _SMHJSEBuilderClass {
    JSEBuilderClass super;
};

void smh_jse_builder_receiver_info(SMHJSEBuilder *self, SMH__PBEDB *db, gboolean full);
void smh_jse_builder_dongle_version(SMHJSEBuilder *self, SMH__PBEDB *db, gboolean full);
void smh_jse_builder_state(SMHJSEBuilder *self, SMH__PBEDB *db, gboolean full);
void smh_jse_builder_check_srls(SMHJSEBuilder *self, SMH__PBEDB *db, GArray *services, gboolean full);
void smh_jse_builder_check_srl(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBService *service, gboolean full);
void smh_jse_builder_get_configuration(SMHJSEBuilder *self, SMH__PBEDB *db, gboolean full);
void smh_jse_builder_rooms(SMHJSEBuilder *self, SMH__PBEDB *db, GArray *rooms, gboolean full);
void smh_jse_builder_room(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBRoom *room, gboolean full);
void smh_jse_builder_groupdevs(SMHJSEBuilder *self, SMH__PBEDB *db, GArray *groups, gboolean full);
void smh_jse_builder_groupdev(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBPeripheral *group, gboolean full);
void smh_jse_builder_devices(SMHJSEBuilder *self, SMH__PBEDB *db, GArray *devices, gboolean output, gboolean full);
void smh_jse_builder_device(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBPeripheral *device, gboolean output, gboolean full);
void smh_jse_builder_clusters(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBPeripheral *peripheral, GArray *clusters, gboolean full);
void smh_jse_builder_cluster(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBPeripheral *peripheral, SMH__PBEDBCluster *cluster, gboolean full);
void smh_jse_builder_attributes(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBPeripheral *peripheral, SMH__PBEDBCluster *cluster, GArray *attributes, gboolean full);
void smh_jse_builder_attribute(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBPeripheral *peripheral, SMH__PBEDBCluster *cluster, SMH__PBEDBAttribute *attribute, gboolean full);
void smh_jse_builder_get_script(SMHJSEBuilder *self, SMH__PBEDB *db, gboolean full);
void smh_jse_builder_scripts(SMHJSEBuilder *self, SMH__PBEDB *db, GArray *scripts, gboolean full);
void smh_jse_builder_script(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBAutomation *script, gboolean full);
void smh_jse_builder_schedulers(SMHJSEBuilder *self, SMH__PBEDB *db, GArray *schedulers, gboolean full);
void smh_jse_builder_scheduler(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBScheduler *scheduler, gboolean full);
void smh_jse_builder_actions(SMHJSEBuilder *self, SMH__PBEDB *db, GArray *actions, gboolean full);
void smh_jse_builder_action(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBAction *action, gboolean full);
void smh_jse_builder_condition_sensors(SMHJSEBuilder *self, SMH__PBEDB *db, GArray *conditions, gboolean full);
void smh_jse_builder_condition_sensor(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBCondition *condition, gboolean full);
void smh_jse_builder_used(SMHJSEBuilder *self, gboolean result);
void smh_jse_builder_get_mode(SMHJSEBuilder *self, SMH__PBEDB *db, gboolean full);
void smh_jse_builder_modes(SMHJSEBuilder *self, SMH__PBEDB *db, GArray *modes, gboolean full);
void smh_jse_builder_mode(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBAutomation *mode, gboolean full);
void smh_jse_builder_configure_buttons(SMHJSEBuilder *self, SMH__PBEDB *db, GArray *devices, gboolean full);
void smh_jse_builder_configure_button(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBPeripheral *device, gboolean full);
void smh_jse_builder_mobile_registrations(SMHJSEBuilder *self, SMH__PBEDB *db, GArray *subscribers, gboolean full);
void smh_jse_builder_mobile_registration(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBSubscriber *subscriber, gboolean full);
void smh_jse_builder_phone_numbers(SMHJSEBuilder *self, SMH__PBEDB *db, GArray *subscribers, gboolean full);
void smh_jse_builder_phone_number(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBSubscriber *subscriber, gboolean full);
void smh_jse_builder_update_devices(SMHJSEBuilder *self, SMH__PBEDB *db, GArray *updates, gboolean full);
void smh_jse_builder_update_device(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBUpdate *update, gboolean full);
void smh_jse_builder_event(SMHJSEBuilder *self, SMH__PBEDBEvent *event);
void smh_jse_builder_default(SMHJSEBuilder *self);

G_END_DECLS

#endif //LIBRARY_SMARTHOME_SMHJSEBUILDER_H
