//
// Created by dan on 11.09.2019.
//

#include "smhsdk.h"
#include "smhsoeserver.h"

int smh_sdk_run(void) {
    (void)ge_main_thread_shared();

    SMHSOEServer *server = smh_soe_server_shared();
    guint ret = smh_soe_server_get_port(server);
    return ret;
}

void smh_sdk_restart(void) {
    SMHSOEServer *server = smh_soe_server_shared();
    smh_soe_server_restart(server);
}
