//
// Created by dan on 20.08.2019.
//

#ifndef LIBRARY_SMARTHOME_SMHPBEDB_H
#define LIBRARY_SMARTHOME_SMHPBEDB_H

#include "smhmain.h"
#include "smhpbedb.pb-c.h"










G_BEGIN_DECLS

#define SMH_TYPE_PBE_DB smh_pbe_db_get_type()
#define SMH_PBE_DB_OBJECT(self) ((SMH__PBEDB *)PBE_DB_OBJECT(self))

G_DECLARE_DERIVABLE_TYPE(SMHPBEDB, smh_pbe_db, SMH, PBE_DB, PBEDB)

struct _SMHPBEDBClass {
    PBEDBClass super;
};

GE_POINTER_SHARED(SMHPBEDB, smh_pbe_db)

G_END_DECLS










G_BEGIN_DECLS

PBE_MESSAGE(smh__pbedbservice, SMH__PBEDBService)
PBE_MESSAGE_STRING(smh__pbedbservice, message, SMH__PBEDBService)

G_END_DECLS










G_BEGIN_DECLS

PBE_MESSAGE(smh__pbedbroom, SMH__PBEDBRoom)
PBE_MESSAGE_STRING(smh__pbedbroom, name, SMH__PBEDBRoom)
PBE_MESSAGE_STRING(smh__pbedbroom, photo_md5, SMH__PBEDBRoom)

G_END_DECLS










G_BEGIN_DECLS

PBE_MESSAGE(smh__pbedbattribute, SMH__PBEDBAttribute)
PBE_MESSAGE_STRING(smh__pbedbattribute, str_value, SMH__PBEDBAttribute)

G_END_DECLS










G_BEGIN_DECLS

PBE_MESSAGE(smh__pbedbcluster, SMH__PBEDBCluster)
PBE_MESSAGE_ARRAY(smh__pbedbcluster, attributes, SMH__PBEDBCluster, SMH__PBEDBAttribute *, smh__pbedbattribute__clear)

G_END_DECLS










G_BEGIN_DECLS

PBE_MESSAGE(smh__pbedbbinding, SMH__PBEDBBinding)

G_END_DECLS










G_BEGIN_DECLS

PBE_MESSAGE(smh__pbedbperipheral, SMH__PBEDBPeripheral)
PBE_MESSAGE_STRING(smh__pbedbperipheral, name, SMH__PBEDBPeripheral)
PBE_MESSAGE_ARRAY(smh__pbedbperipheral, clusters, SMH__PBEDBPeripheral, SMH__PBEDBCluster *, smh__pbedbcluster__clear)
PBE_MESSAGE_ARRAY(smh__pbedbperipheral, bindings, SMH__PBEDBPeripheral, SMH__PBEDBBinding *, smh__pbedbbinding__clear)

G_END_DECLS










G_BEGIN_DECLS

PBE_MESSAGE(smh__pbedbautomation, SMH__PBEDBAutomation)
PBE_MESSAGE_STRING(smh__pbedbautomation, name, SMH__PBEDBAutomation)

G_END_DECLS










G_BEGIN_DECLS

PBE_MESSAGE(smh__pbedbscheduler, SMH__PBEDBScheduler)
PBE_MESSAGE_STRING(smh__pbedbscheduler, start_time, SMH__PBEDBScheduler)
PBE_MESSAGE_STRING(smh__pbedbscheduler, end_time, SMH__PBEDBScheduler)
PBE_MESSAGE_ARRAY(smh__pbedbscheduler, weekdays, SMH__PBEDBScheduler, SMH__PBEDBScheduler__Weekday, NULL)

G_END_DECLS










G_BEGIN_DECLS

PBE_MESSAGE(smh__pbedbaction, SMH__PBEDBAction)
PBE_MESSAGE_ARRAY(smh__pbedbaction, parameters, SMH__PBEDBAction, gint32, NULL)
PBE_MESSAGE_ARRAY(smh__pbedbaction, devices, SMH__PBEDBAction, guint32, NULL)
PBE_MESSAGE_ARRAY(smh__pbedbaction, groups, SMH__PBEDBAction, guint32, NULL)

G_END_DECLS










G_BEGIN_DECLS

PBE_MESSAGE(smh__pbedbcondition, SMH__PBEDBCondition)

G_END_DECLS










G_BEGIN_DECLS

PBE_MESSAGE(smh__pbedbsubscriber, SMH__PBEDBSubscriber)
PBE_MESSAGE_STRING(smh__pbedbsubscriber, registration_token, SMH__PBEDBSubscriber)
PBE_MESSAGE_STRING(smh__pbedbsubscriber, hw_id, SMH__PBEDBSubscriber)
PBE_MESSAGE_STRING(smh__pbedbsubscriber, name, SMH__PBEDBSubscriber)
PBE_MESSAGE_STRING(smh__pbedbsubscriber, os, SMH__PBEDBSubscriber)
PBE_MESSAGE_STRING(smh__pbedbsubscriber, phone_number, SMH__PBEDBSubscriber)

G_END_DECLS










G_BEGIN_DECLS

PBE_MESSAGE(smh__pbedbupdate, SMH__PBEDBUpdate)

G_END_DECLS










G_BEGIN_DECLS

PBE_MESSAGE(smh__pbedb, SMH__PBEDB)
PBE_MESSAGE_STRING(smh__pbedb, model_name, SMH__PBEDB)
PBE_MESSAGE_STRING(smh__pbedb, user_friendly_model_name, SMH__PBEDB)
PBE_MESSAGE_STRING(smh__pbedb, hardware_id, SMH__PBEDB)
PBE_MESSAGE_STRING(smh__pbedb, software_version, SMH__PBEDB)
PBE_MESSAGE_STRING(smh__pbedb, serial_number, SMH__PBEDB)
PBE_MESSAGE_STRING(smh__pbedb, dre_id, SMH__PBEDB)
PBE_MESSAGE_STRING(smh__pbedb, api_version, SMH__PBEDB)
PBE_MESSAGE_STRING(smh__pbedb, version, SMH__PBEDB)
PBE_MESSAGE_STRING(smh__pbedb, hw_revision, SMH__PBEDB)
PBE_MESSAGE_STRING(smh__pbedb, active_client, SMH__PBEDB)
PBE_MESSAGE_STRING(smh__pbedb, session_id, SMH__PBEDB)
PBE_MESSAGE_ARRAY(smh__pbedb, services, SMH__PBEDB, SMH__PBEDBService *, smh__pbedbservice__clear)
PBE_MESSAGE_ARRAY(smh__pbedb, rooms, SMH__PBEDB, SMH__PBEDBRoom *, smh__pbedbroom__clear)
PBE_MESSAGE_ARRAY(smh__pbedb, peripherals, SMH__PBEDB, SMH__PBEDBPeripheral *, smh__pbedbperipheral__clear)
PBE_MESSAGE_ARRAY(smh__pbedb, automations, SMH__PBEDB, SMH__PBEDBAutomation *, smh__pbedbautomation__clear)
PBE_MESSAGE_ARRAY(smh__pbedb, schedulers, SMH__PBEDB, SMH__PBEDBScheduler *, smh__pbedbscheduler__clear)
PBE_MESSAGE_ARRAY(smh__pbedb, actions, SMH__PBEDB, SMH__PBEDBAction *, smh__pbedbaction__clear)
PBE_MESSAGE_ARRAY(smh__pbedb, conditions, SMH__PBEDB, SMH__PBEDBCondition *, smh__pbedbcondition__clear)
PBE_MESSAGE_ARRAY(smh__pbedb, subscribers, SMH__PBEDB, SMH__PBEDBSubscriber *, smh__pbedbsubscriber__clear)
PBE_MESSAGE_ARRAY(smh__pbedb, updates, SMH__PBEDB, SMH__PBEDBUpdate *, smh__pbedbupdate__clear)

G_END_DECLS










#endif //LIBRARY_SMARTHOME_SMHPBEDB_H
