//
// Created by dan on 11.08.2019.
//

#ifndef LIBRARY_SMARTHOME_SMHSOESERVER_H
#define LIBRARY_SMARTHOME_SMHSOESERVER_H

#include "smhmain.h"
#include "smhjsebuilder.h"
#include "smhjsegenerator.h"
#include "smhjsereader.h"
#include "smhjseparser.h"
#include "smhmanager.h"

G_BEGIN_DECLS

#define SMH_TYPE_SOE_SERVER smh_soe_server_get_type()

G_DECLARE_DERIVABLE_TYPE(SMHSOEServer, smh_soe_server, SMH, SOE_SERVER, SOEServer)

struct _SMHSOEServerClass {
    SOEServerClass super;
};

GE_OBJECT_PROPERTY(smh_soe_server, port, SMHSOEServer, guint)
GE_OBJECT_PROPERTY(smh_soe_server, messages, SMHSOEServer, GArray *)

GE_POINTER_SHARED(SMHSOEServer, smh_soe_server)

void smh_soe_server_restart(SMHSOEServer *self);

G_END_DECLS

#endif //LIBRARY_SMARTHOME_SMHSOESERVER_H
