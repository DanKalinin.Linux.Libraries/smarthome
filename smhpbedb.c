//
// Created by dan on 20.08.2019.
//

#include "smhpbedb.h"
#include "smhi18n.h"

typedef struct {
    gpointer padding[1];
} SMHPBEDBPrivate;

static gboolean smh_pbe_db_missing(PBEDB *self, GError **error);

G_DEFINE_TYPE_WITH_CODE(SMHPBEDB, smh_pbe_db, PBE_TYPE_DB, G_ADD_PRIVATE(SMHPBEDB))

GE_POINTER_SHARED_C(SMHPBEDB, smh_pbe_db)

static void smh_pbe_db_class_init(SMHPBEDBClass *class) {
    PBE_DB_CLASS(class)->missing = smh_pbe_db_missing;
}

static void smh_pbe_db_init(SMHPBEDB *self) {
    pbe_db_set_descriptor(PBE_DB(self), &smh__pbedb__descriptor);

    const gchar *home = g_get_home_dir();
    g_autofree gchar *documents = g_build_filename(home, "Documents", NULL);
    pbe_db_set_directory(PBE_DB(self), documents);

    pbe_db_read(PBE_DB(self), NULL);
}

static gboolean smh_pbe_db_missing(PBEDB *self, GError **error) {
    SMH__PBEDB *object = smh__pbedb__new();
    smh__pbedb__set_model_name(object, "b533m");
    smh__pbedb__set_user_friendly_model_name(object, "Demo");
    smh__pbedb__set_hardware_id(object, "rev01");
    smh__pbedb__set_software_version(object, "1.0.68");
    smh__pbedb__set_serial_number(object, "00000000000000000000001");
    object->offset = 3;
    smh__pbedb__set_dre_id(object, "36040902725304");
    smh__pbedb__set_api_version(object, "1.5");
    smh__pbedb__set_version(object, "2.7");
    smh__pbedb__set_hw_revision(object, "5.2");

    // Service

    GArray *services = smh__pbedb__get_services(object);

    SMH__PBEDBService *service = smh__pbedbservice__new();
    service->id = 1;
    service->state = 1;
    g_array_append_val(services, service);

    service = smh__pbedbservice__new();
    service->id = 2;
    service->state = 1;
    g_array_append_val(services, service);

    service = smh__pbedbservice__new();
    service->id = 3;
    service->state = 0;
    smh__pbedbservice__set_message(service, _("SMS notification service is not connected (error 317)"));
    g_array_append_val(services, service);

    service = smh__pbedbservice__new();
    service->id = 4;
    service->state = 1;
    g_array_append_val(services, service);

    service = smh__pbedbservice__new();
    service->id = 5;
    service->state = 1;
    g_array_append_val(services, service);

    service = smh__pbedbservice__new();
    service->id = 6;
    service->state = 1;
    g_array_append_val(services, service);

    service = smh__pbedbservice__new();
    service->id = 7;
    service->state = 1;
    g_array_append_val(services, service);

    smh__pbedb__set_services(object, services);

    // Peripheral

    GArray *peripherals = smh__pbedb__get_peripherals(object);
    
    SMH__PBEDBPeripheral *peripheral = smh__pbedbperipheral__new();
    peripheral->type = SMH__PBEDBPERIPHERAL__TYPE__TYPE_DEVICE;
    peripheral->id = ++object->peripheral_id;
    smh__pbedbperipheral__set_name(peripheral, _("Color lamp"));
    peripheral->device_type = SMH__PBEDBPERIPHERAL__DEVICE_TYPE__DEVICE_TYPE_RGB_CCT;
    peripheral->undev = TRUE;
    peripheral->undev_address = peripheral->id;

    GArray *clusters = smh__pbedbperipheral__get_clusters(peripheral);

    // Cluster - Basic

    SMH__PBEDBCluster *cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_BASIC;

    GArray *attributes = smh__pbedbcluster__get_attributes(cluster);

    SMH__PBEDBAttribute *attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_MANUFACTURER;
    smh__pbedbattribute__set_str_value(attribute, "GS");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_MODEL;
    smh__pbedbattribute__set_str_value(attribute, "BRHM8E27W70-I1");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_POWER_SOURCE;
    attribute->str_value = g_strdup_printf("%i", SMH__PBEDBATTRIBUTE__POWER_SOURCE__POWER_SOURCE_MAINS_1);
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Identify

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_IDENTIFY;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_IDENTIFY_TIME;
    smh__pbedbattribute__set_str_value(attribute, "0");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Groups

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_GROUPS;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_GROUPS_GROUPS;
    smh__pbedbattribute__set_str_value(attribute, "0");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - On/Off

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_ON_OFF;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_ON_OFF_VALUE;
    smh__pbedbattribute__set_str_value(attribute, "1");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - OTA

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_OTA;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_OTA_VERSION;
    smh__pbedbattribute__set_str_value(attribute, "1.0");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Color

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_COLOR;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_COLOR_HUE;
    smh__pbedbattribute__set_str_value(attribute, "24");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_COLOR_SATURATION;
    smh__pbedbattribute__set_str_value(attribute, "216");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_COLOR_TEMPERATURE;
    smh__pbedbattribute__set_str_value(attribute, "360");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_COLOR_MODE;
    attribute->str_value = g_strdup_printf("%i", SMH__PBEDBATTRIBUTE__COLOR_MODE__COLOR_MODE_TEMPERATURE);
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Level

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_LEVEL;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_LEVEL_VALUE;
    smh__pbedbattribute__set_str_value(attribute, "128");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Heiman RGB CCT

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_HEIMAN_RGB_CCT;

    attributes = smh__pbedbcluster__get_attributes(cluster);
    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    smh__pbedbperipheral__set_clusters(peripheral, clusters);

    g_array_append_val(peripherals, peripheral);

    //

    peripheral = smh__pbedbperipheral__new();
    peripheral->type = SMH__PBEDBPERIPHERAL__TYPE__TYPE_DEVICE;
    peripheral->id = ++object->peripheral_id;
    smh__pbedbperipheral__set_name(peripheral, _("Color lamp-1"));
    peripheral->device_type = SMH__PBEDBPERIPHERAL__DEVICE_TYPE__DEVICE_TYPE_RGB_CCT;
    peripheral->undev = TRUE;
    peripheral->undev_address = peripheral->id;

    clusters = smh__pbedbperipheral__get_clusters(peripheral);

    // Cluster - Basic

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_BASIC;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_MANUFACTURER;
    smh__pbedbattribute__set_str_value(attribute, "GS");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_MODEL;
    smh__pbedbattribute__set_str_value(attribute, "BRHM8E27W70-I1");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_POWER_SOURCE;
    attribute->str_value = g_strdup_printf("%i", SMH__PBEDBATTRIBUTE__POWER_SOURCE__POWER_SOURCE_MAINS_1);
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Identify

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_IDENTIFY;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_IDENTIFY_TIME;
    smh__pbedbattribute__set_str_value(attribute, "0");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Groups

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_GROUPS;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_GROUPS_GROUPS;
    smh__pbedbattribute__set_str_value(attribute, "0");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - On/Off

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_ON_OFF;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_ON_OFF_VALUE;
    smh__pbedbattribute__set_str_value(attribute, "1");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - OTA

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_OTA;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_OTA_VERSION;
    smh__pbedbattribute__set_str_value(attribute, "1.0");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Color

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_COLOR;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_COLOR_HUE;
    smh__pbedbattribute__set_str_value(attribute, "24");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_COLOR_SATURATION;
    smh__pbedbattribute__set_str_value(attribute, "216");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_COLOR_TEMPERATURE;
    smh__pbedbattribute__set_str_value(attribute, "360");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_COLOR_MODE;
    attribute->str_value = g_strdup_printf("%i", SMH__PBEDBATTRIBUTE__COLOR_MODE__COLOR_MODE_TEMPERATURE);
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Level

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_LEVEL;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_LEVEL_VALUE;
    smh__pbedbattribute__set_str_value(attribute, "128");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Heiman RGB CCT

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_HEIMAN_RGB_CCT;

    attributes = smh__pbedbcluster__get_attributes(cluster);
    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    smh__pbedbperipheral__set_clusters(peripheral, clusters);

    g_array_append_val(peripherals, peripheral);

    //

    peripheral = smh__pbedbperipheral__new();
    peripheral->type = SMH__PBEDBPERIPHERAL__TYPE__TYPE_DEVICE;
    peripheral->id = ++object->peripheral_id;
    smh__pbedbperipheral__set_name(peripheral, _("Lamp"));
    peripheral->device_type = SMH__PBEDBPERIPHERAL__DEVICE_TYPE__DEVICE_TYPE_CCT;
    peripheral->undev = TRUE;
    peripheral->undev_address = peripheral->id;

    clusters = smh__pbedbperipheral__get_clusters(peripheral);

    // Cluster - Basic

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_BASIC;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_MANUFACTURER;
    smh__pbedbattribute__set_str_value(attribute, "GS");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_MODEL;
    smh__pbedbattribute__set_str_value(attribute, "BDHM8E27W70-I1");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_POWER_SOURCE;
    attribute->str_value = g_strdup_printf("%i", SMH__PBEDBATTRIBUTE__POWER_SOURCE__POWER_SOURCE_MAINS_1);
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Identify

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_IDENTIFY;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_IDENTIFY_TIME;
    smh__pbedbattribute__set_str_value(attribute, "0");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Groups

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_GROUPS;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_GROUPS_GROUPS;
    smh__pbedbattribute__set_str_value(attribute, "0");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - On/Off

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_ON_OFF;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_ON_OFF_VALUE;
    smh__pbedbattribute__set_str_value(attribute, "1");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - OTA

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_OTA;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_OTA_VERSION;
    smh__pbedbattribute__set_str_value(attribute, "1.0");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Color

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_COLOR;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_COLOR_TEMPERATURE;
    smh__pbedbattribute__set_str_value(attribute, "360");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_COLOR_MODE;
    attribute->str_value = g_strdup_printf("%i", SMH__PBEDBATTRIBUTE__COLOR_MODE__COLOR_MODE_TEMPERATURE);
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Level

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_LEVEL;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_LEVEL_VALUE;
    smh__pbedbattribute__set_str_value(attribute, "128");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Heiman CCT

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_HEIMAN_CCT;

    attributes = smh__pbedbcluster__get_attributes(cluster);
    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    smh__pbedbperipheral__set_clusters(peripheral, clusters);

    g_array_append_val(peripherals, peripheral);

    //

    peripheral = smh__pbedbperipheral__new();
    peripheral->type = SMH__PBEDBPERIPHERAL__TYPE__TYPE_DEVICE;
    peripheral->id = ++object->peripheral_id;
    smh__pbedbperipheral__set_name(peripheral, _("Socket"));
    peripheral->device_type = SMH__PBEDBPERIPHERAL__DEVICE_TYPE__DEVICE_TYPE_SOCKET;
    peripheral->undev = TRUE;
    peripheral->undev_address = peripheral->id;

    clusters = smh__pbedbperipheral__get_clusters(peripheral);

    // Cluster - Basic

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_BASIC;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_MANUFACTURER;
    smh__pbedbattribute__set_str_value(attribute, "GS");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_MODEL;
    smh__pbedbattribute__set_str_value(attribute, "SKHMP30-I1");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_POWER_SOURCE;
    attribute->str_value = g_strdup_printf("%i", SMH__PBEDBATTRIBUTE__POWER_SOURCE__POWER_SOURCE_MAINS_1);
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Groups

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_GROUPS;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_GROUPS_GROUPS;
    smh__pbedbattribute__set_str_value(attribute, "0");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - On/Off

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_ON_OFF;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_ON_OFF_VALUE;
    smh__pbedbattribute__set_str_value(attribute, "1");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Heiman Socket

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_HEIMAN_SOCKET;

    attributes = smh__pbedbcluster__get_attributes(cluster);
    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    smh__pbedbperipheral__set_clusters(peripheral, clusters);

    g_array_append_val(peripherals, peripheral);

    //

    peripheral = smh__pbedbperipheral__new();
    peripheral->type = SMH__PBEDBPERIPHERAL__TYPE__TYPE_DEVICE;
    peripheral->id = ++object->peripheral_id;
    smh__pbedbperipheral__set_name(peripheral, _("Socket-1"));
    peripheral->device_type = SMH__PBEDBPERIPHERAL__DEVICE_TYPE__DEVICE_TYPE_SOCKET;
    peripheral->undev = TRUE;
    peripheral->undev_address = peripheral->id;

    clusters = smh__pbedbperipheral__get_clusters(peripheral);

    // Cluster - Basic

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_BASIC;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_MANUFACTURER;
    smh__pbedbattribute__set_str_value(attribute, "GS");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_MODEL;
    smh__pbedbattribute__set_str_value(attribute, "SKHMP30-I1");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_POWER_SOURCE;
    attribute->str_value = g_strdup_printf("%i", SMH__PBEDBATTRIBUTE__POWER_SOURCE__POWER_SOURCE_MAINS_1);
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Groups

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_GROUPS;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_GROUPS_GROUPS;
    smh__pbedbattribute__set_str_value(attribute, "0");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - On/Off

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_ON_OFF;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_ON_OFF_VALUE;
    smh__pbedbattribute__set_str_value(attribute, "1");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Heiman Socket

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_HEIMAN_SOCKET;

    attributes = smh__pbedbcluster__get_attributes(cluster);
    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    smh__pbedbperipheral__set_clusters(peripheral, clusters);

    g_array_append_val(peripherals, peripheral);

    //

    peripheral = smh__pbedbperipheral__new();
    peripheral->type = SMH__PBEDBPERIPHERAL__TYPE__TYPE_DEVICE;
    peripheral->id = ++object->peripheral_id;
    smh__pbedbperipheral__set_name(peripheral, _("Temperature sensor"));
    peripheral->device_type = SMH__PBEDBPERIPHERAL__DEVICE_TYPE__DEVICE_TYPE_TEMPERATURE;
    peripheral->undev = TRUE;
    peripheral->undev_address = peripheral->id;

    clusters = smh__pbedbperipheral__get_clusters(peripheral);

    // Cluster - Basic

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_BASIC;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_MANUFACTURER;
    smh__pbedbattribute__set_str_value(attribute, "GS");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_MODEL;
    smh__pbedbattribute__set_str_value(attribute, "STHM-I1H");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_POWER_SOURCE;
    attribute->str_value = g_strdup_printf("%i", SMH__PBEDBATTRIBUTE__POWER_SOURCE__POWER_SOURCE_BATTERY);
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Power

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_POWER;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_POWER_PERCENTAGE;
    smh__pbedbattribute__set_str_value(attribute, "150");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Temperature

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_TEMPERATURE;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_TEMPERATURE_VALUE;
    smh__pbedbattribute__set_str_value(attribute, "2500");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_TEMPERATURE_MIN;
    smh__pbedbattribute__set_str_value(attribute, "-1500");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_TEMPERATURE_MAX;
    smh__pbedbattribute__set_str_value(attribute, "6000");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Humidity

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_HUMIDITY;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_HUMIDITY_VALUE;
    smh__pbedbattribute__set_str_value(attribute, "5000");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_HUMIDITY_MIN;
    smh__pbedbattribute__set_str_value(attribute, "0");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_HUMIDITY_MAX;
    smh__pbedbattribute__set_str_value(attribute, "10000");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - OTA

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_OTA;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_OTA_VERSION;
    smh__pbedbattribute__set_str_value(attribute, "1.0");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    smh__pbedbperipheral__set_clusters(peripheral, clusters);

    g_array_append_val(peripherals, peripheral);

    //

    peripheral = smh__pbedbperipheral__new();
    peripheral->type = SMH__PBEDBPERIPHERAL__TYPE__TYPE_DEVICE;
    peripheral->id = ++object->peripheral_id;
    smh__pbedbperipheral__set_name(peripheral, _("Keychain"));
    peripheral->device_type = SMH__PBEDBPERIPHERAL__DEVICE_TYPE__DEVICE_TYPE_KEYCHAIN;
    peripheral->undev = TRUE;
    peripheral->undev_address = peripheral->id;

    clusters = smh__pbedbperipheral__get_clusters(peripheral);

    // Cluster - Basic

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_BASIC;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_MANUFACTURER;
    smh__pbedbattribute__set_str_value(attribute, "GS");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_MODEL;
    smh__pbedbattribute__set_str_value(attribute, "KFHMS-I1");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_POWER_SOURCE;
    attribute->str_value = g_strdup_printf("%i", SMH__PBEDBATTRIBUTE__POWER_SOURCE__POWER_SOURCE_BATTERY);
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Power

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_POWER;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_POWER_PERCENTAGE;
    smh__pbedbattribute__set_str_value(attribute, "150");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - IAS

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_IAS;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_IAS_STATUS;
    smh__pbedbattribute__set_str_value(attribute, "0");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Heiman Keychain

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_HEIMAN_KEYCHAIN;

    attributes = smh__pbedbcluster__get_attributes(cluster);
    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    smh__pbedbperipheral__set_clusters(peripheral, clusters);

    g_array_append_val(peripherals, peripheral);

    //

    peripheral = smh__pbedbperipheral__new();
    peripheral->type = SMH__PBEDBPERIPHERAL__TYPE__TYPE_DEVICE;
    peripheral->id = ++object->peripheral_id;
    smh__pbedbperipheral__set_name(peripheral, _("Siren"));
    peripheral->device_type = SMH__PBEDBPERIPHERAL__DEVICE_TYPE__DEVICE_TYPE_SIREN;
    peripheral->undev = TRUE;
    peripheral->undev_address = peripheral->id;

    clusters = smh__pbedbperipheral__get_clusters(peripheral);

    // Cluster - Basic

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_BASIC;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_MANUFACTURER;
    smh__pbedbattribute__set_str_value(attribute, "GS");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_MODEL;
    smh__pbedbattribute__set_str_value(attribute, "SRHMP-I1");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_POWER_SOURCE;
    attribute->str_value = g_strdup_printf("%i", (SMH__PBEDBATTRIBUTE__POWER_SOURCE__POWER_SOURCE_MAINS_1 | SMH__PBEDBATTRIBUTE__POWER_SOURCE__POWER_SOURCE_BACKUP));
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Power

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_POWER;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_POWER_PERCENTAGE;
    smh__pbedbattribute__set_str_value(attribute, "150");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - IAS

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_IAS;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_IAS_STATUS;
    attribute->str_value = g_strdup_printf("%i", SMH__PBEDBATTRIBUTE__IASSTATUS__IAS_STATUS_NONE);
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Siren

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_SIREN;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_SIREN_MAX_DURATION;
    smh__pbedbattribute__set_str_value(attribute, "240");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Heiman Siren

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_HEIMAN_SIREN;

    attributes = smh__pbedbcluster__get_attributes(cluster);
    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    smh__pbedbperipheral__set_clusters(peripheral, clusters);

    g_array_append_val(peripherals, peripheral);

    //

    peripheral = smh__pbedbperipheral__new();
    peripheral->type = SMH__PBEDBPERIPHERAL__TYPE__TYPE_DEVICE;
    peripheral->id = ++object->peripheral_id;
    smh__pbedbperipheral__set_name(peripheral, _("Motion sensor"));
    peripheral->device_type = SMH__PBEDBPERIPHERAL__DEVICE_TYPE__DEVICE_TYPE_IAS;
    peripheral->undev = TRUE;
    peripheral->undev_address = peripheral->id;

    clusters = smh__pbedbperipheral__get_clusters(peripheral);

    // Cluster - Basic

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_BASIC;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_MANUFACTURER;
    smh__pbedbattribute__set_str_value(attribute, "GS");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_MODEL;
    smh__pbedbattribute__set_str_value(attribute, "SWHM-I1");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_POWER_SOURCE;
    attribute->str_value = g_strdup_printf("%i", SMH__PBEDBATTRIBUTE__POWER_SOURCE__POWER_SOURCE_BATTERY);
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Power

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_POWER;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_POWER_PERCENTAGE;
    smh__pbedbattribute__set_str_value(attribute, "200");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - IAS

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_IAS;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_IAS_TYPE;
    smh__pbedbattribute__set_str_value(attribute, "13");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_IAS_STATUS;
    smh__pbedbattribute__set_str_value(attribute, "37");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Heiman Motion

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_HEIMAN_MOTION;

    attributes = smh__pbedbcluster__get_attributes(cluster);
    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    smh__pbedbperipheral__set_clusters(peripheral, clusters);

    g_array_append_val(peripherals, peripheral);

    //

    peripheral = smh__pbedbperipheral__new();
    peripheral->type = SMH__PBEDBPERIPHERAL__TYPE__TYPE_DEVICE;
    peripheral->id = ++object->peripheral_id;
    smh__pbedbperipheral__set_name(peripheral, _("Door sensor"));
    peripheral->device_type = SMH__PBEDBPERIPHERAL__DEVICE_TYPE__DEVICE_TYPE_IAS;
    peripheral->undev = TRUE;
    peripheral->undev_address = peripheral->id;

    clusters = smh__pbedbperipheral__get_clusters(peripheral);

    // Cluster - Basic

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_BASIC;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_MANUFACTURER;
    smh__pbedbattribute__set_str_value(attribute, "GS");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_MODEL;
    smh__pbedbattribute__set_str_value(attribute, "SWHM-I1");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_POWER_SOURCE;
    attribute->str_value = g_strdup_printf("%i", SMH__PBEDBATTRIBUTE__POWER_SOURCE__POWER_SOURCE_BATTERY);
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Power

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_POWER;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_POWER_PERCENTAGE;
    smh__pbedbattribute__set_str_value(attribute, "200");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - IAS

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_IAS;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_IAS_TYPE;
    smh__pbedbattribute__set_str_value(attribute, "21");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_IAS_STATUS;
    smh__pbedbattribute__set_str_value(attribute, "32");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Heiman Door

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_HEIMAN_DOOR;

    attributes = smh__pbedbcluster__get_attributes(cluster);
    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    smh__pbedbperipheral__set_clusters(peripheral, clusters);

    g_array_append_val(peripherals, peripheral);

    //

    peripheral = smh__pbedbperipheral__new();
    peripheral->type = SMH__PBEDBPERIPHERAL__TYPE__TYPE_DEVICE;
    peripheral->id = ++object->peripheral_id;
    smh__pbedbperipheral__set_name(peripheral, _("Leakage sensor"));
    peripheral->device_type = SMH__PBEDBPERIPHERAL__DEVICE_TYPE__DEVICE_TYPE_IAS;
    peripheral->undev = TRUE;
    peripheral->undev_address = peripheral->id;

    clusters = smh__pbedbperipheral__get_clusters(peripheral);

    // Cluster - Basic

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_BASIC;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_MANUFACTURER;
    smh__pbedbattribute__set_str_value(attribute, "GS");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_MODEL;
    smh__pbedbattribute__set_str_value(attribute, "SWHM-I1");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_POWER_SOURCE;
    attribute->str_value = g_strdup_printf("%i", SMH__PBEDBATTRIBUTE__POWER_SOURCE__POWER_SOURCE_BATTERY);
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Power

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_POWER;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_POWER_PERCENTAGE;
    smh__pbedbattribute__set_str_value(attribute, "200");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - IAS

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_IAS;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_IAS_TYPE;
    smh__pbedbattribute__set_str_value(attribute, "42");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_IAS_STATUS;
    smh__pbedbattribute__set_str_value(attribute, "32");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Heiman Leakage

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_HEIMAN_LEAKAGE;

    attributes = smh__pbedbcluster__get_attributes(cluster);
    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    smh__pbedbperipheral__set_clusters(peripheral, clusters);

    g_array_append_val(peripherals, peripheral);

    //

    peripheral = smh__pbedbperipheral__new();
    peripheral->type = SMH__PBEDBPERIPHERAL__TYPE__TYPE_DEVICE;
    peripheral->id = ++object->peripheral_id;
    smh__pbedbperipheral__set_name(peripheral, _("Smoke sensor"));
    peripheral->device_type = SMH__PBEDBPERIPHERAL__DEVICE_TYPE__DEVICE_TYPE_IAS;
    peripheral->undev = TRUE;
    peripheral->undev_address = peripheral->id;

    clusters = smh__pbedbperipheral__get_clusters(peripheral);

    // Cluster - Basic

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_BASIC;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_MANUFACTURER;
    smh__pbedbattribute__set_str_value(attribute, "GS");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_MODEL;
    smh__pbedbattribute__set_str_value(attribute, "SWHM-I1");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_POWER_SOURCE;
    attribute->str_value = g_strdup_printf("%i", SMH__PBEDBATTRIBUTE__POWER_SOURCE__POWER_SOURCE_BATTERY);
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Power

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_POWER;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_POWER_PERCENTAGE;
    smh__pbedbattribute__set_str_value(attribute, "200");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - IAS

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_IAS;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_IAS_TYPE;
    smh__pbedbattribute__set_str_value(attribute, "40");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_IAS_STATUS;
    smh__pbedbattribute__set_str_value(attribute, "32");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Heiman Smoke

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_HEIMAN_SMOKE;

    attributes = smh__pbedbcluster__get_attributes(cluster);
    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    smh__pbedbperipheral__set_clusters(peripheral, clusters);

    g_array_append_val(peripherals, peripheral);

    //

    peripheral = smh__pbedbperipheral__new();
    peripheral->type = SMH__PBEDBPERIPHERAL__TYPE__TYPE_DEVICE;
    peripheral->id = ++object->peripheral_id;
    smh__pbedbperipheral__set_name(peripheral, _("Gas sensor"));
    peripheral->device_type = SMH__PBEDBPERIPHERAL__DEVICE_TYPE__DEVICE_TYPE_IAS;
    peripheral->undev = TRUE;
    peripheral->undev_address = peripheral->id;

    clusters = smh__pbedbperipheral__get_clusters(peripheral);

    // Cluster - Basic

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_BASIC;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_MANUFACTURER;
    smh__pbedbattribute__set_str_value(attribute, "GS");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_MODEL;
    smh__pbedbattribute__set_str_value(attribute, "SWHM-I1");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_BASIC_POWER_SOURCE;
    attribute->str_value = g_strdup_printf("%i", SMH__PBEDBATTRIBUTE__POWER_SOURCE__POWER_SOURCE_BATTERY);
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Power

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_POWER;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_POWER_PERCENTAGE;
    smh__pbedbattribute__set_str_value(attribute, "200");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - IAS

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_IAS;

    attributes = smh__pbedbcluster__get_attributes(cluster);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_IAS_TYPE;
    smh__pbedbattribute__set_str_value(attribute, "43");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    attribute = smh__pbedbattribute__new();
    attribute->key = SMH__PBEDBATTRIBUTE__KEY__KEY_IAS_STATUS;
    smh__pbedbattribute__set_str_value(attribute, "0");
    attribute->num_value = TRUE;
    g_array_append_val(attributes, attribute);

    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    // Cluster - Heiman Gas

    cluster = smh__pbedbcluster__new();
    cluster->id = SMH__PBEDBCLUSTER__ID__ID_HEIMAN_GAS;

    attributes = smh__pbedbcluster__get_attributes(cluster);
    smh__pbedbcluster__set_attributes(cluster, attributes);

    g_array_append_val(clusters, cluster);

    smh__pbedbperipheral__set_clusters(peripheral, clusters);

    g_array_append_val(peripherals, peripheral);

    smh__pbedb__set_peripherals(object, peripherals);

    pbe_db_set_object(self, (ProtobufCMessage *)object);

    gboolean ret = pbe_db_write(self, error);
    return ret;
}

SMHPBEDB *_smh_pbe_db_shared(void) {
    SMHPBEDB *self = g_object_new(SMH_TYPE_PBE_DB, NULL);
    return self;
}
