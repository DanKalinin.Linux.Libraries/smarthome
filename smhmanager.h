//
// Created by dan on 05.09.2019.
//

#ifndef LIBRARY_SMARTHOME_SMHMANAGER_H
#define LIBRARY_SMARTHOME_SMHMANAGER_H

#include "smhmain.h"
#include "smhpbedb.h"

G_BEGIN_DECLS

#define SMH_TYPE_MANAGER smh_manager_get_type()

G_DECLARE_DERIVABLE_TYPE(SMHManager, smh_manager, SMH, MANAGER, GEObject)

struct _SMHManagerClass {
    GEObjectClass super;

    void (*peripheral_value_set)(SMHManager *self, SMH__PBEDB *db, SMH__PBEDBPeripheral *peripheral, SMH__PBEDBCluster *cluster, SMH__PBEDBAttribute *attribute);
    void (*automation_on_set)(SMHManager *self, SMH__PBEDB *db, SMH__PBEDBAutomation *automation);
};

GE_POINTER_SHARED(SMHManager, smh_manager)

void smh_manager_peripheral_value_set(SMHManager *self, SMH__PBEDB *db, SMH__PBEDBPeripheral *peripheral, SMH__PBEDBCluster *cluster, SMH__PBEDBAttribute *attribute);
void smh_manager_automation_on_set(SMHManager *self, SMH__PBEDB *db, SMH__PBEDBAutomation *automation);

void smh_manager_peripheral_send_command(SMHManager *self, SMH__PBEDB *db, SMH__PBEDBPeripheral *peripheral, SMH__PBEDBCluster *cluster, SMH__PBEDBCommand *command);
void smh_manager_peripheral_set_value(SMHManager *self, SMH__PBEDB *db, SMH__PBEDBPeripheral *peripheral, SMH__PBEDBCluster *cluster, SMH__PBEDBAttribute *attribute);
void smh_manager_automation_set_on(SMHManager *self, SMH__PBEDB *db, SMH__PBEDBAutomation *automation);

G_END_DECLS

#endif //LIBRARY_SMARTHOME_SMHMANAGER_H
