//
// Created by dan on 11.08.2019.
//

#include "smhavhentrygroup.h"

typedef struct {
    gpointer padding[1];
} SMHAVHEntryGroupPrivate;

G_DEFINE_TYPE_WITH_CODE(SMHAVHEntryGroup, smh_avh_entry_group, AVH_TYPE_ENTRY_GROUP, G_ADD_PRIVATE(SMHAVHEntryGroup))

GE_POINTER_SHARED_C(SMHAVHEntryGroup, smh_avh_entry_group)

static void smh_avh_entry_group_class_init(SMHAVHEntryGroupClass *class) {

}

static void smh_avh_entry_group_init(SMHAVHEntryGroup *self) {
    SMHAVHClient *client = smh_avh_client_shared();

    AvahiEntryGroup *object = avahi_entry_group_new(AVH_CLIENT_OBJECT(client), avh_entry_group_state_callback, self);
    avh_entry_group_set_object(AVH_ENTRY_GROUP(self), object);

    SMHSOEServer *server = smh_soe_server_shared();
    guint port = smh_soe_server_get_port(server);

    (void)avahi_entry_group_add_service(object, AVAHI_IF_UNSPEC, AVAHI_PROTO_INET, 0, "00000000000000000000000", "_smarthome-remote._tcp", NULL, NULL, port, "model=Demo", "txtvers=1", NULL);
    (void)avahi_entry_group_commit(object);
}

SMHAVHEntryGroup *_smh_avh_entry_group_shared(void) {
    SMHAVHEntryGroup *self = g_object_new(SMH_TYPE_AVH_ENTRY_GROUP, NULL);
    return self;
}
