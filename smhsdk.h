//
// Created by dan on 11.09.2019.
//

#ifndef LIBRARY_SMARTHOME_SMHSDK_H
#define LIBRARY_SMARTHOME_SMHSDK_H

int smh_sdk_run(void);
void smh_sdk_restart(void);

#endif //LIBRARY_SMARTHOME_SMHSDK_H
