//
// Created by dan on 11.08.2019.
//

#include "smhsoeserver.h"

typedef struct {
    guint port;
    GArray *messages;
} SMHSOEServerPrivate;

static void smh_soe_server_constructed(GObject *self);
static void smh_soe_server_receiver_info(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task);
static void smh_soe_server_dongle_version(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task);
static void smh_soe_server_state(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task);
static void smh_soe_server_check_srl(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task);
static void smh_soe_server_get_configuration(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task);
static void smh_soe_server_room(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task);
static void smh_soe_server_groupdev(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task);
static void smh_soe_server_unknown_device(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task);
static void smh_soe_server_device(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task);
static void smh_soe_server_opportunity(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task);
static void smh_soe_server_get_script(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task);
static void smh_soe_server_script(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task);
static void smh_soe_server_scheduler(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task);
static void smh_soe_server_action(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task);
static void smh_soe_server_condition_sensor(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task);
static void smh_soe_server_used_devices(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task);
static void smh_soe_server_used_recipients(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task);
static void smh_soe_server_mode(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task);
static void smh_soe_server_run_mode(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task);
static void smh_soe_server_configure_button(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task);
static void smh_soe_server_mobile_registration(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task);
static void smh_soe_server_phone_number(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task);
static void smh_soe_server_update_device(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task);
static void smh_soe_server_sync_status(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task);
static void smh_soe_server_default(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task);
static void smh_soe_server_soup_message_finished(SoupMessage *sender, gpointer self);
static void smh_soe_server_smh_manager_peripheral_value_set(SMHManager *sender, SMH__PBEDB *db, SMH__PBEDBPeripheral *peripheral, SMH__PBEDBCluster *cluster, SMH__PBEDBAttribute *attribute, gpointer self);
static void smh_soe_server_smh_manager_automation_on_set(SMHManager *sender, SMH__PBEDB *db, SMH__PBEDBAutomation *automation, gpointer self);
static void smh_soe_server_send_event(SMHSOEServer *self, SMH__PBEDBEvent *event);

G_DEFINE_TYPE_WITH_CODE(SMHSOEServer, smh_soe_server, SOE_TYPE_SERVER, G_ADD_PRIVATE(SMHSOEServer))

GE_OBJECT_PROPERTY_C(smh_soe_server, port, SMHSOEServer, guint)
GE_OBJECT_PROPERTY_POINTER_C(smh_soe_server, messages, SMHSOEServer, GArray, g_array_ref, g_array_unref, NULL)

GE_POINTER_SHARED_C(SMHSOEServer, smh_soe_server)

static void smh_soe_server_class_init(SMHSOEServerClass *class) {
    G_OBJECT_CLASS(class)->constructed = smh_soe_server_constructed;
}

static void smh_soe_server_init(SMHSOEServer *self) {

}

static void smh_soe_server_constructed(GObject *self) {
    G_OBJECT_CLASS(smh_soe_server_parent_class)->constructed(self);

    (void)soup_server_listen_all(SOUP_SERVER(self), 60000, 0, NULL);
//    (void)soup_server_listen_local(SOUP_SERVER(self), 0, SOUP_SERVER_LISTEN_IPV4_ONLY, NULL);

    g_autoslist(SoupURI) uris = soup_server_get_uris(SOUP_SERVER(self));
    guint port = soup_uri_get_port(uris->data);
    smh_soe_server_set_port(SMH_SOE_SERVER(self), port);

    g_autoptr(GArray) messages = ge_array_new(NULL, 0, sizeof(SoupMessage *), NULL);
    smh_soe_server_set_messages(SMH_SOE_SERVER(self), messages);

    SMHManager *manager = smh_manager_shared();
    g_signal_connect(manager, "peripheral-value-set", G_CALLBACK(smh_soe_server_smh_manager_peripheral_value_set), self);
    g_signal_connect(manager, "automation-on-set", G_CALLBACK(smh_soe_server_smh_manager_automation_on_set), self);

    soup_server_add_handler(SOUP_SERVER(self), "/v1.3/smarthome/receiver-info", smh_soe_server_receiver_info, NULL, NULL);
    soup_server_add_handler(SOUP_SERVER(self), "/v1.3/smarthome/dongle_version", smh_soe_server_dongle_version, NULL, NULL);
    soup_server_add_handler(SOUP_SERVER(self), "/v1.3/smarthome/state", smh_soe_server_state, NULL, NULL);
    soup_server_add_handler(SOUP_SERVER(self), "/v1.3/smarthome/check_srl", smh_soe_server_check_srl, NULL, NULL);
    soup_server_add_handler(SOUP_SERVER(self), "/v1.3/smarthome/get_configuration", smh_soe_server_get_configuration, NULL, NULL);
    soup_server_add_handler(SOUP_SERVER(self), "/v1.3/smarthome/room", smh_soe_server_room, NULL, NULL);
    soup_server_add_handler(SOUP_SERVER(self), "/v1.3/smarthome/groupdev", smh_soe_server_groupdev, NULL, NULL);
    soup_server_add_handler(SOUP_SERVER(self), "/v1.3/smarthome/unknown_device", smh_soe_server_unknown_device, NULL, NULL);
    soup_server_add_handler(SOUP_SERVER(self), "/v1.3/smarthome/device", smh_soe_server_device, NULL, NULL);
    soup_server_add_handler(SOUP_SERVER(self), "/v1.3/smarthome/opportunity", smh_soe_server_opportunity, NULL, NULL);
    soup_server_add_handler(SOUP_SERVER(self), "/v1.3/smarthome/scripts/get_script", smh_soe_server_get_script, NULL, NULL);
    soup_server_add_handler(SOUP_SERVER(self), "/v1.3/smarthome/script", smh_soe_server_script, NULL, NULL);
    soup_server_add_handler(SOUP_SERVER(self), "/v1.3/smarthome/scripts/scheduler", smh_soe_server_scheduler, NULL, NULL);
    soup_server_add_handler(SOUP_SERVER(self), "/v1.3/smarthome/scripts/action", smh_soe_server_action, NULL, NULL);
    soup_server_add_handler(SOUP_SERVER(self), "/v1.3/smarthome/scripts/condition_sensor", smh_soe_server_condition_sensor, NULL, NULL);
    soup_server_add_handler(SOUP_SERVER(self), "/v1.3/smarthome/scripts/get_scripts/used_devices", smh_soe_server_used_devices, NULL, NULL);
    soup_server_add_handler(SOUP_SERVER(self), "/v1.3/smarthome/scripts/get_scripts/used_recipients", smh_soe_server_used_recipients, NULL, NULL);
    soup_server_add_handler(SOUP_SERVER(self), "/v1.3/smarthome/mode", smh_soe_server_mode, NULL, NULL);
    soup_server_add_handler(SOUP_SERVER(self), "/v1.3/smarthome/run_mode", smh_soe_server_run_mode, NULL, NULL);
    soup_server_add_handler(SOUP_SERVER(self), "/v1.3/smarthome/configure_button", smh_soe_server_configure_button, NULL, NULL);
    soup_server_add_handler(SOUP_SERVER(self), "/v1.3/smarthome/mobile_registration", smh_soe_server_mobile_registration, NULL, NULL);
    soup_server_add_handler(SOUP_SERVER(self), "/v1.3/smarthome/phone_number", smh_soe_server_phone_number, NULL, NULL);
    soup_server_add_handler(SOUP_SERVER(self), "/v1.3/smarthome/update_device", smh_soe_server_update_device, NULL, NULL);
    soup_server_add_handler(SOUP_SERVER(self), "/v1.3/smarthome/sync_statuses", smh_soe_server_sync_status, NULL, NULL);
    soup_server_add_handler(SOUP_SERVER(self), NULL, smh_soe_server_default, NULL, NULL);
}

static void smh_soe_server_receiver_info(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task) {
    if (message->method == SOUP_METHOD_GET) {
        SMHPBEDB *db = smh_pbe_db_shared();

        g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
        smh_jse_builder_receiver_info(builder, SMH_PBE_DB_OBJECT(db), TRUE);
        g_autoptr(JsonNode) root = json_builder_get_root(JSON_BUILDER(builder));

        g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", root, NULL);
        gsize length = 0;
        gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

        soup_message_set_status(message, SOUP_STATUS_OK);
        soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    } else {
        soup_message_set_status(message, SOUP_STATUS_NOT_IMPLEMENTED);
    }

    g_print("%s %s\n", message->method, path);
}

static void smh_soe_server_dongle_version(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task) {
    if (message->method == SOUP_METHOD_GET) {
        SMHPBEDB *db = smh_pbe_db_shared();

        g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
        smh_jse_builder_dongle_version(builder, SMH_PBE_DB_OBJECT(db), TRUE);
        g_autoptr(JsonNode) root = json_builder_get_root(JSON_BUILDER(builder));

        g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", root, NULL);
        gsize length = 0;
        gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

        soup_message_set_status(message, SOUP_STATUS_OK);
        soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    } else {
        soup_message_set_status(message, SOUP_STATUS_NOT_IMPLEMENTED);
    }

    g_print("%s %s\n", message->method, path);
}

static void smh_soe_server_state(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task) {
    if (message->method == SOUP_METHOD_GET) {
        SMHPBEDB *db = smh_pbe_db_shared();

        g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
        smh_jse_builder_state(builder, SMH_PBE_DB_OBJECT(db), TRUE);
        g_autoptr(JsonNode) root = json_builder_get_root(JSON_BUILDER(builder));

        g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", root, NULL);
        gsize length = 0;
        gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

        soup_message_set_status(message, SOUP_STATUS_OK);
        soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    } else if (message->method == SOUP_METHOD_PUT) {
        SMHPBEDB *db = smh_pbe_db_shared();

        g_autoptr(SMHJSEParser) parser = g_object_new(SMH_TYPE_JSE_PARSER, NULL);
        (void)json_parser_load_from_data(JSON_PARSER(parser), message->request_body->data, message->request_body->length, NULL);
        JsonNode *request_root = json_parser_get_root(JSON_PARSER(parser));

        g_autoptr(SMHJSEReader) reader = g_object_new(SMH_TYPE_JSE_READER, "root", request_root, NULL);
        smh_jse_reader_state(reader, SMH_PBE_DB_OBJECT(db));

        (void)pbe_db_write(PBE_DB(db), NULL);

        g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
        smh_jse_builder_state(builder, SMH_PBE_DB_OBJECT(db), FALSE);
        g_autoptr(JsonNode) response_root = json_builder_get_root(JSON_BUILDER(builder));

        g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", response_root, NULL);
        gsize length = 0;
        gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

        soup_message_set_status(message, SOUP_STATUS_OK);
        soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    } else {
        soup_message_set_status(message, SOUP_STATUS_NOT_IMPLEMENTED);
    }

    g_print("%s %s\n", message->method, path);
}

static void smh_soe_server_check_srl(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task) {
    if (message->method == SOUP_METHOD_GET) {
        SMHPBEDB *db = smh_pbe_db_shared();

        g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
        g_autoptr(GArray) services = smh__pbedb__ref_services(SMH_PBE_DB_OBJECT(db));
        smh_jse_builder_check_srls(builder, SMH_PBE_DB_OBJECT(db), services, TRUE);
        g_autoptr(JsonNode) root = json_builder_get_root(JSON_BUILDER(builder));

        g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", root, NULL);
        gsize length = 0;
        gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

        soup_message_set_status(message, SOUP_STATUS_OK);
        soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    } else {
        soup_message_set_status(message, SOUP_STATUS_NOT_IMPLEMENTED);
    }

    g_print("%s %s\n", message->method, path);
}

static void smh_soe_server_get_configuration(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task) {
    if (message->method == SOUP_METHOD_GET) {
        SMHPBEDB *db = smh_pbe_db_shared();

        g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
        smh_jse_builder_get_configuration(builder, SMH_PBE_DB_OBJECT(db), TRUE);
        g_autoptr(JsonNode) root = json_builder_get_root(JSON_BUILDER(builder));

        g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", root, NULL);
        gsize length = 0;
        gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

        soup_message_set_status(message, SOUP_STATUS_OK);
        soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    } else {
        soup_message_set_status(message, SOUP_STATUS_NOT_IMPLEMENTED);
    }

    g_print("%s %s\n", message->method, path);
}

static void smh_soe_server_room(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task) {
    if (message->method == SOUP_METHOD_GET) {
        SMHPBEDB *db = smh_pbe_db_shared();

        g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
        g_autoptr(GArray) rooms = smh__pbedb__ref_rooms(SMH_PBE_DB_OBJECT(db));

        if (query == NULL) {
        } else {
            gchar *id = g_hash_table_lookup(query, "room_id");
            if (id == NULL) {
            } else {
                GE_ARRAY_FILTER(rooms, i, e, SMH__PBEDBRoom *, (e->id == atoi(id)));
            }
        }

        smh_jse_builder_rooms(builder, SMH_PBE_DB_OBJECT(db), rooms, FALSE);
        g_autoptr(JsonNode) root = json_builder_get_root(JSON_BUILDER(builder));

        g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", root, NULL);
        gsize length = 0;
        gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

        soup_message_set_status(message, SOUP_STATUS_OK);
        soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    } else if (message->method == SOUP_METHOD_POST) {
        SMHPBEDB *db = smh_pbe_db_shared();

        GArray *rooms = smh__pbedb__get_rooms(SMH_PBE_DB_OBJECT(db));

        SMH__PBEDBRoom *room = smh__pbedbroom__new();
        room->id = ++SMH_PBE_DB_OBJECT(db)->room_id;

        g_autoptr(SMHJSEParser) parser = g_object_new(SMH_TYPE_JSE_PARSER, NULL);
        (void)json_parser_load_from_data(JSON_PARSER(parser), message->request_body->data, message->request_body->length, NULL);
        JsonNode *request_root = json_parser_get_root(JSON_PARSER(parser));

        g_autoptr(SMHJSEReader) reader = g_object_new(SMH_TYPE_JSE_READER, "root", request_root, NULL);
        smh_jse_reader_room(reader, SMH_PBE_DB_OBJECT(db), room);

        g_array_append_val(rooms, room);

        smh__pbedb__set_rooms(SMH_PBE_DB_OBJECT(db), rooms);

        (void)pbe_db_write(PBE_DB(db), NULL);

        g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
        smh_jse_builder_room(builder, SMH_PBE_DB_OBJECT(db), room, FALSE);
        g_autoptr(JsonNode) response_root = json_builder_get_root(JSON_BUILDER(builder));

        g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", response_root, NULL);
        gsize length = 0;
        gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

        soup_message_set_status(message, SOUP_STATUS_OK);
        soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    } else if (message->method == SOUP_METHOD_PUT) {
        SMHPBEDB *db = smh_pbe_db_shared();

        g_autoptr(GArray) rooms = smh__pbedb__ref_rooms(SMH_PBE_DB_OBJECT(db));

        gchar *id = g_hash_table_lookup(query, "room_id");
        GE_ARRAY_FILTER(rooms, i, e, SMH__PBEDBRoom *, (e->id == atoi(id)));
        if (rooms->len > 0) {
            SMH__PBEDBRoom *room = g_array_index(rooms, SMH__PBEDBRoom *, 0);

            g_autoptr(SMHJSEParser) parser = g_object_new(SMH_TYPE_JSE_PARSER, NULL);
            (void)json_parser_load_from_data(JSON_PARSER(parser), message->request_body->data, message->request_body->length, NULL);
            JsonNode *request_root = json_parser_get_root(JSON_PARSER(parser));

            g_autoptr(SMHJSEReader) reader = g_object_new(SMH_TYPE_JSE_READER, "root", request_root, NULL);
            smh_jse_reader_room(reader, SMH_PBE_DB_OBJECT(db), room);

            (void)pbe_db_write(PBE_DB(db), NULL);

            g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
            smh_jse_builder_room(builder, SMH_PBE_DB_OBJECT(db), room, FALSE);
            g_autoptr(JsonNode) response_root = json_builder_get_root(JSON_BUILDER(builder));

            g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", response_root, NULL);
            gsize length = 0;
            gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

            soup_message_set_status(message, SOUP_STATUS_OK);
            soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
        }
    } else if (message->method == SOUP_METHOD_DELETE) {
        SMHPBEDB *db = smh_pbe_db_shared();

        GArray *rooms = smh__pbedb__get_rooms(SMH_PBE_DB_OBJECT(db));

        gchar *id = g_hash_table_lookup(query, "room_id");
        GE_ARRAY_FILTER(rooms, i, e, SMH__PBEDBRoom *, (e->id != atoi(id)));

        smh__pbedb__set_rooms(SMH_PBE_DB_OBJECT(db), rooms);

        (void)pbe_db_write(PBE_DB(db), NULL);

        soup_message_set_status(message, SOUP_STATUS_OK);
    } else {
        soup_message_set_status(message, SOUP_STATUS_NOT_IMPLEMENTED);
    }

    g_print("%s %s\n", message->method, path);
}

static void smh_soe_server_groupdev(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task) {
    if (message->method == SOUP_METHOD_GET) {
        SMHPBEDB *db = smh_pbe_db_shared();

        g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
        g_autoptr(GArray) groups = smh__pbedb__ref_peripherals(SMH_PBE_DB_OBJECT(db));
        GE_ARRAY_FILTER(groups, i, e, SMH__PBEDBPeripheral *, (e->type == SMH__PBEDBPERIPHERAL__TYPE__TYPE_GROUP));

        if (query == NULL) {
        } else {
            gchar *id = g_hash_table_lookup(query, "group_id");
            if (id == NULL) {
            } else {
                GE_ARRAY_FILTER(groups, i, e, SMH__PBEDBPeripheral *, (e->id == atoi(id)));
            }
        }

        smh_jse_builder_groupdevs(builder, SMH_PBE_DB_OBJECT(db), groups, FALSE);
        g_autoptr(JsonNode) root = json_builder_get_root(JSON_BUILDER(builder));

        g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", root, NULL);
        gsize length = 0;
        gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

        soup_message_set_status(message, SOUP_STATUS_OK);
        soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    } else if (message->method == SOUP_METHOD_POST) {
        SMHPBEDB *db = smh_pbe_db_shared();

        GArray *peripherals = smh__pbedb__get_peripherals(SMH_PBE_DB_OBJECT(db));

        SMH__PBEDBPeripheral *group = smh__pbedbperipheral__new();
        group->type = SMH__PBEDBPERIPHERAL__TYPE__TYPE_GROUP;
        group->id = ++SMH_PBE_DB_OBJECT(db)->peripheral_id;

        g_autoptr(SMHJSEParser) parser = g_object_new(SMH_TYPE_JSE_PARSER, NULL);
        (void)json_parser_load_from_data(JSON_PARSER(parser), message->request_body->data, message->request_body->length, NULL);
        JsonNode *request_root = json_parser_get_root(JSON_PARSER(parser));

        g_autoptr(SMHJSEReader) reader = g_object_new(SMH_TYPE_JSE_READER, "root", request_root, NULL);
        smh_jse_reader_groupdev(reader, SMH_PBE_DB_OBJECT(db), group);

        g_array_append_val(peripherals, group);

        smh__pbedb__set_peripherals(SMH_PBE_DB_OBJECT(db), peripherals);

        (void)pbe_db_write(PBE_DB(db), NULL);

        g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
        smh_jse_builder_groupdev(builder, SMH_PBE_DB_OBJECT(db), group, FALSE);
        g_autoptr(JsonNode) response_root = json_builder_get_root(JSON_BUILDER(builder));

        g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", response_root, NULL);
        gsize length = 0;
        gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

        soup_message_set_status(message, SOUP_STATUS_OK);
        soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    } else if (message->method == SOUP_METHOD_PUT) {
        SMHPBEDB *db = smh_pbe_db_shared();

        g_autoptr(GArray) peripherals = smh__pbedb__ref_peripherals(SMH_PBE_DB_OBJECT(db));

        gchar *id = g_hash_table_lookup(query, "group_id");
        GE_ARRAY_FILTER(peripherals, i, e, SMH__PBEDBPeripheral *, (e->id == atoi(id)));
        if (peripherals->len > 0) {
            SMH__PBEDBPeripheral *group = g_array_index(peripherals, SMH__PBEDBPeripheral *, 0);

            g_autoptr(SMHJSEParser) parser = g_object_new(SMH_TYPE_JSE_PARSER, NULL);
            (void)json_parser_load_from_data(JSON_PARSER(parser), message->request_body->data, message->request_body->length, NULL);
            JsonNode *request_root = json_parser_get_root(JSON_PARSER(parser));

            g_autoptr(SMHJSEReader) reader = g_object_new(SMH_TYPE_JSE_READER, "root", request_root, NULL);
            smh_jse_reader_groupdev(reader, SMH_PBE_DB_OBJECT(db), group);

            (void)pbe_db_write(PBE_DB(db), NULL);

            g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
            smh_jse_builder_groupdev(builder, SMH_PBE_DB_OBJECT(db), group, FALSE);
            g_autoptr(JsonNode) response_root = json_builder_get_root(JSON_BUILDER(builder));

            g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", response_root, NULL);
            gsize length = 0;
            gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

            soup_message_set_status(message, SOUP_STATUS_OK);
            soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
        }
    } else if (message->method == SOUP_METHOD_DELETE) {
        SMHPBEDB *db = smh_pbe_db_shared();

        GArray *peripherals = smh__pbedb__get_peripherals(SMH_PBE_DB_OBJECT(db));

        gchar *id = g_hash_table_lookup(query, "group_id");
        GE_ARRAY_FILTER(peripherals, i, e, SMH__PBEDBPeripheral *, (e->id != atoi(id)));

        smh__pbedb__set_peripherals(SMH_PBE_DB_OBJECT(db), peripherals);

        (void)pbe_db_write(PBE_DB(db), NULL);

        soup_message_set_status(message, SOUP_STATUS_OK);
    } else {
        soup_message_set_status(message, SOUP_STATUS_NOT_IMPLEMENTED);
    }

    g_print("%s %s\n", message->method, path);
}

static void smh_soe_server_unknown_device(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task) {
    if (message->method == SOUP_METHOD_GET) {
        SMHPBEDB *db = smh_pbe_db_shared();

        g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
        g_autoptr(GArray) devices = smh__pbedb__ref_peripherals(SMH_PBE_DB_OBJECT(db));
        GE_ARRAY_FILTER(devices, i, e, SMH__PBEDBPeripheral *, ((e->type == SMH__PBEDBPERIPHERAL__TYPE__TYPE_DEVICE) && (e->undev == TRUE)));
        smh_jse_builder_devices(builder, SMH_PBE_DB_OBJECT(db), devices, FALSE, TRUE);
        g_autoptr(JsonNode) root = json_builder_get_root(JSON_BUILDER(builder));

        g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", root, NULL);
        gsize length = 0;
        gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

        soup_message_set_status(message, SOUP_STATUS_OK);
        soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    } else {
        soup_message_set_status(message, SOUP_STATUS_NOT_IMPLEMENTED);
    }

    g_print("%s %s\n", message->method, path);
}

static void smh_soe_server_device(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task) {
    if (message->method == SOUP_METHOD_GET) {
        SMHPBEDB *db = smh_pbe_db_shared();

        g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
        g_autoptr(GArray) devices = smh__pbedb__ref_peripherals(SMH_PBE_DB_OBJECT(db));
        GE_ARRAY_FILTER(devices, i, e, SMH__PBEDBPeripheral *, ((e->type == SMH__PBEDBPERIPHERAL__TYPE__TYPE_DEVICE) && (e->undev == FALSE)));

        if (query == NULL) {
        } else {
            gchar *id = g_hash_table_lookup(query, "dev_id");
            if (id == NULL) {
            } else {
                GE_ARRAY_FILTER(devices, i, e, SMH__PBEDBPeripheral *, (e->id == atoi(id)));
            }
        }

        gboolean output = ((query != NULL) && g_hash_table_contains(query, "output_clstr"));
        smh_jse_builder_devices(builder, SMH_PBE_DB_OBJECT(db), devices, output, TRUE);
        g_autoptr(JsonNode) root = json_builder_get_root(JSON_BUILDER(builder));

        g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", root, NULL);
        gsize length = 0;
        gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

        soup_message_set_status(message, SOUP_STATUS_OK);
        soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    } else if (message->method == SOUP_METHOD_PUT) {
        SMHPBEDB *db = smh_pbe_db_shared();

        g_autoptr(GArray) peripherals = smh__pbedb__ref_peripherals(SMH_PBE_DB_OBJECT(db));

        gchar *id = g_hash_table_lookup(query, "dev_id");
        GE_ARRAY_FILTER(peripherals, i, e, SMH__PBEDBPeripheral *, (e->id == atoi(id)));
        if (peripherals->len > 0) {
            SMH__PBEDBPeripheral *device = g_array_index(peripherals, SMH__PBEDBPeripheral *, 0);
            device->undev = FALSE;

            g_autoptr(SMHJSEParser) parser = g_object_new(SMH_TYPE_JSE_PARSER, NULL);
            (void)json_parser_load_from_data(JSON_PARSER(parser), message->request_body->data, message->request_body->length, NULL);
            JsonNode *request_root = json_parser_get_root(JSON_PARSER(parser));

            g_autoptr(SMHJSEReader) reader = g_object_new(SMH_TYPE_JSE_READER, "root", request_root, NULL);
            smh_jse_reader_device(reader, SMH_PBE_DB_OBJECT(db), device);

            (void)pbe_db_write(PBE_DB(db), NULL);

            g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
            smh_jse_builder_device(builder, SMH_PBE_DB_OBJECT(db), device, FALSE, FALSE);
            g_autoptr(JsonNode) response_root = json_builder_get_root(JSON_BUILDER(builder));

            g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", response_root, NULL);
            gsize length = 0;
            gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

            soup_message_set_status(message, SOUP_STATUS_OK);
            soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
        }
    } else if (message->method == SOUP_METHOD_DELETE) {
        SMHPBEDB *db = smh_pbe_db_shared();

        g_autoptr(GArray) peripherals = smh__pbedb__ref_peripherals(SMH_PBE_DB_OBJECT(db));

        gchar *id = g_hash_table_lookup(query, "dev_id");
        GE_ARRAY_FILTER(peripherals, i, e, SMH__PBEDBPeripheral *, (e->id == atoi(id)));
        if (peripherals->len > 0) {
            SMH__PBEDBPeripheral *device = g_array_index(peripherals, SMH__PBEDBPeripheral *, 0);
            device->undev = TRUE;

            GArray *bindings = smh__pbedbperipheral__get_bindings(device);
            g_array_set_size(bindings, 0);
            smh__pbedbperipheral__set_bindings(device, bindings);

            (void)pbe_db_write(PBE_DB(db), NULL);
        }

        soup_message_set_status(message, SOUP_STATUS_OK);
    } else {
        soup_message_set_status(message, SOUP_STATUS_NOT_IMPLEMENTED);
    }

    g_print("%s %s\n", message->method, path);
}

static void smh_soe_server_opportunity(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task) {
    if (message->method == SOUP_METHOD_POST) {
        SMHPBEDB *db = smh_pbe_db_shared();

        g_autoptr(SMHJSEParser) parser = g_object_new(SMH_TYPE_JSE_PARSER, NULL);
        (void)json_parser_load_from_data(JSON_PARSER(parser), message->request_body->data, message->request_body->length, NULL);
        JsonNode *request_root = json_parser_get_root(JSON_PARSER(parser));

        g_autoptr(SMHJSEReader) reader = g_object_new(SMH_TYPE_JSE_READER, "root", request_root, NULL);
        smh_jse_reader_opportunity(reader, SMH_PBE_DB_OBJECT(db));

        (void)pbe_db_write(PBE_DB(db), NULL);

        soup_message_set_status(message, SOUP_STATUS_OK);
    } else {
        soup_message_set_status(message, SOUP_STATUS_NOT_IMPLEMENTED);
    }

    g_print("body - %s\n", message->request_body->data);
    g_print("%s %s\n", message->method, path);
}

static void smh_soe_server_get_script(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task) {
    if (message->method == SOUP_METHOD_GET) {
        SMHPBEDB *db = smh_pbe_db_shared();

        g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
        smh_jse_builder_get_script(builder, SMH_PBE_DB_OBJECT(db), TRUE);
        g_autoptr(JsonNode) root = json_builder_get_root(JSON_BUILDER(builder));

        g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", root, NULL);
        gsize length = 0;
        gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

        soup_message_set_status(message, SOUP_STATUS_OK);
        soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    } else {
        soup_message_set_status(message, SOUP_STATUS_NOT_IMPLEMENTED);
    }

    g_print("%s %s\n", message->method, path);
}

static void smh_soe_server_script(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task) {
    if (message->method == SOUP_METHOD_GET) {
        SMHPBEDB *db = smh_pbe_db_shared();

        g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
        g_autoptr(GArray) scripts = smh__pbedb__ref_automations(SMH_PBE_DB_OBJECT(db));
        GE_ARRAY_FILTER(scripts, i, e, SMH__PBEDBAutomation *, (e->type == SMH__PBEDBAUTOMATION__TYPE__TYPE_SCRIPT));
        smh_jse_builder_scripts(builder, SMH_PBE_DB_OBJECT(db), scripts, TRUE);
        g_autoptr(JsonNode) root = json_builder_get_root(JSON_BUILDER(builder));

        g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", root, NULL);
        gsize length = 0;
        gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

        soup_message_set_status(message, SOUP_STATUS_OK);
        soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    } else if (message->method == SOUP_METHOD_POST) {
        SMHPBEDB *db = smh_pbe_db_shared();

        GArray *automations = smh__pbedb__get_automations(SMH_PBE_DB_OBJECT(db));

        SMH__PBEDBAutomation *script = smh__pbedbautomation__new();
        script->type = SMH__PBEDBAUTOMATION__TYPE__TYPE_SCRIPT;
        script->id = ++SMH_PBE_DB_OBJECT(db)->automation_id;

        g_autoptr(SMHJSEParser) parser = g_object_new(SMH_TYPE_JSE_PARSER, NULL);
        (void)json_parser_load_from_data(JSON_PARSER(parser), message->request_body->data, message->request_body->length, NULL);
        JsonNode *request_root = json_parser_get_root(JSON_PARSER(parser));

        g_autoptr(SMHJSEReader) reader = g_object_new(SMH_TYPE_JSE_READER, "root", request_root, NULL);
        smh_jse_reader_script(reader, SMH_PBE_DB_OBJECT(db), script);

        g_array_append_val(automations, script);

        smh__pbedb__set_automations(SMH_PBE_DB_OBJECT(db), automations);

        (void)pbe_db_write(PBE_DB(db), NULL);

        g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
        smh_jse_builder_script(builder, SMH_PBE_DB_OBJECT(db), script, FALSE);
        g_autoptr(JsonNode) response_root = json_builder_get_root(JSON_BUILDER(builder));

        g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", response_root, NULL);
        gsize length = 0;
        gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

        soup_message_set_status(message, SOUP_STATUS_OK);
        soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    } else if (message->method == SOUP_METHOD_PUT) {
        SMHPBEDB *db = smh_pbe_db_shared();

        g_autoptr(GArray) automations = smh__pbedb__ref_automations(SMH_PBE_DB_OBJECT(db));

        gchar *id = g_hash_table_lookup(query, "scrt_id");
        GE_ARRAY_FILTER(automations, i, e, SMH__PBEDBAutomation *, (e->id == atoi(id)));
        if (automations->len > 0) {
            SMH__PBEDBAutomation *script = g_array_index(automations, SMH__PBEDBAutomation *, 0);

            g_autoptr(SMHJSEParser) parser = g_object_new(SMH_TYPE_JSE_PARSER, NULL);
            (void)json_parser_load_from_data(JSON_PARSER(parser), message->request_body->data, message->request_body->length, NULL);
            JsonNode *request_root = json_parser_get_root(JSON_PARSER(parser));

            g_autoptr(SMHJSEReader) reader = g_object_new(SMH_TYPE_JSE_READER, "root", request_root, NULL);
            smh_jse_reader_script(reader, SMH_PBE_DB_OBJECT(db), script);

            (void)pbe_db_write(PBE_DB(db), NULL);

            SMHManager *manager = smh_manager_shared();
            smh_manager_automation_set_on(manager, SMH_PBE_DB_OBJECT(db), script);

            g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
            smh_jse_builder_script(builder, SMH_PBE_DB_OBJECT(db), script, FALSE);
            g_autoptr(JsonNode) response_root = json_builder_get_root(JSON_BUILDER(builder));

            g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", response_root, NULL);
            gsize length = 0;
            gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

            soup_message_set_status(message, SOUP_STATUS_OK);
            soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
        }
    } else if (message->method == SOUP_METHOD_DELETE) {
        SMHPBEDB *db = smh_pbe_db_shared();

        GArray *automations = smh__pbedb__get_automations(SMH_PBE_DB_OBJECT(db));
        GArray *schedulers = smh__pbedb__get_schedulers(SMH_PBE_DB_OBJECT(db));
        GArray *actions = smh__pbedb__get_actions(SMH_PBE_DB_OBJECT(db));
        GArray *conditions = smh__pbedb__get_conditions(SMH_PBE_DB_OBJECT(db));

        gchar *id = g_hash_table_lookup(query, "scrt_id");
        GE_ARRAY_FILTER(automations, i, e, SMH__PBEDBAutomation *, (e->id != atoi(id)));
        GE_ARRAY_FILTER(schedulers, i, e, SMH__PBEDBScheduler *, (e->automation != atoi(id)));
        GE_ARRAY_FILTER(actions, i, e, SMH__PBEDBAction *, (e->automation != atoi(id)));
        GE_ARRAY_FILTER(conditions, i, e, SMH__PBEDBCondition *, (e->automation != atoi(id)));

        smh__pbedb__set_automations(SMH_PBE_DB_OBJECT(db), automations);
        smh__pbedb__set_schedulers(SMH_PBE_DB_OBJECT(db), schedulers);
        smh__pbedb__set_actions(SMH_PBE_DB_OBJECT(db), actions);
        smh__pbedb__set_conditions(SMH_PBE_DB_OBJECT(db), conditions);

        (void)pbe_db_write(PBE_DB(db), NULL);

        soup_message_set_status(message, SOUP_STATUS_OK);
    } else {
        soup_message_set_status(message, SOUP_STATUS_NOT_IMPLEMENTED);
    }

    g_print("%s %s\n", message->method, path);
}

static void smh_soe_server_scheduler(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task) {
    if (message->method == SOUP_METHOD_GET) {
        SMHPBEDB *db = smh_pbe_db_shared();

        g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
        g_autoptr(GArray) schedulers = smh__pbedb__ref_schedulers(SMH_PBE_DB_OBJECT(db));
        GE_ARRAY_FILTER(schedulers, i, e, SMH__PBEDBScheduler *, (e->type == SMH__PBEDBAUTOMATION__TYPE__TYPE_SCRIPT));
        smh_jse_builder_schedulers(builder, SMH_PBE_DB_OBJECT(db), schedulers, TRUE);
        g_autoptr(JsonNode) root = json_builder_get_root(JSON_BUILDER(builder));

        g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", root, NULL);
        gsize length = 0;
        gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

        soup_message_set_status(message, SOUP_STATUS_OK);
        soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    } else if (message->method == SOUP_METHOD_POST) {
        SMHPBEDB *db = smh_pbe_db_shared();

        GArray *schedulers = smh__pbedb__get_schedulers(SMH_PBE_DB_OBJECT(db));

        SMH__PBEDBScheduler *scheduler = smh__pbedbscheduler__new();
        scheduler->type = SMH__PBEDBAUTOMATION__TYPE__TYPE_SCRIPT;
        scheduler->id = ++SMH_PBE_DB_OBJECT(db)->scheduler_id;

        g_autoptr(SMHJSEParser) parser = g_object_new(SMH_TYPE_JSE_PARSER, NULL);
        (void)json_parser_load_from_data(JSON_PARSER(parser), message->request_body->data, message->request_body->length, NULL);
        JsonNode *request_root = json_parser_get_root(JSON_PARSER(parser));

        g_autoptr(SMHJSEReader) reader = g_object_new(SMH_TYPE_JSE_READER, "root", request_root, NULL);
        smh_jse_reader_scheduler(reader, SMH_PBE_DB_OBJECT(db), scheduler);

        g_array_append_val(schedulers, scheduler);

        smh__pbedb__set_schedulers(SMH_PBE_DB_OBJECT(db), schedulers);

        (void)pbe_db_write(PBE_DB(db), NULL);

        g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
        smh_jse_builder_scheduler(builder, SMH_PBE_DB_OBJECT(db), scheduler, FALSE);
        g_autoptr(JsonNode) response_root = json_builder_get_root(JSON_BUILDER(builder));

        g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", response_root, NULL);
        gsize length = 0;
        gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

        soup_message_set_status(message, SOUP_STATUS_OK);
        soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    } else if (message->method == SOUP_METHOD_PUT) {
        SMHPBEDB *db = smh_pbe_db_shared();

        g_autoptr(GArray) schedulers = smh__pbedb__ref_schedulers(SMH_PBE_DB_OBJECT(db));

        gchar *id = g_hash_table_lookup(query, "cond_id");
        GE_ARRAY_FILTER(schedulers, i, e, SMH__PBEDBScheduler *, (e->id == atoi(id)));
        if (schedulers->len > 0) {
            SMH__PBEDBScheduler *scheduler = g_array_index(schedulers, SMH__PBEDBScheduler *, 0);

            g_autoptr(SMHJSEParser) parser = g_object_new(SMH_TYPE_JSE_PARSER, NULL);
            (void)json_parser_load_from_data(JSON_PARSER(parser), message->request_body->data, message->request_body->length, NULL);
            JsonNode *request_root = json_parser_get_root(JSON_PARSER(parser));

            g_autoptr(SMHJSEReader) reader = g_object_new(SMH_TYPE_JSE_READER, "root", request_root, NULL);
            smh_jse_reader_scheduler(reader, SMH_PBE_DB_OBJECT(db), scheduler);

            (void)pbe_db_write(PBE_DB(db), NULL);

            g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
            smh_jse_builder_scheduler(builder, SMH_PBE_DB_OBJECT(db), scheduler, FALSE);
            g_autoptr(JsonNode) response_root = json_builder_get_root(JSON_BUILDER(builder));

            g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", response_root, NULL);
            gsize length = 0;
            gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

            soup_message_set_status(message, SOUP_STATUS_OK);
            soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
        }
    } else if (message->method == SOUP_METHOD_DELETE) {
        SMHPBEDB *db = smh_pbe_db_shared();

        GArray *schedulers = smh__pbedb__get_schedulers(SMH_PBE_DB_OBJECT(db));

        gchar *id = g_hash_table_lookup(query, "cond_id");
        GE_ARRAY_FILTER(schedulers, i, e, SMH__PBEDBScheduler *, (e->id != atoi(id)));

        smh__pbedb__set_schedulers(SMH_PBE_DB_OBJECT(db), schedulers);

        (void)pbe_db_write(PBE_DB(db), NULL);

        soup_message_set_status(message, SOUP_STATUS_OK);
    } else {
        soup_message_set_status(message, SOUP_STATUS_NOT_IMPLEMENTED);
    }

    g_print("%s %s\n", message->method, path);
}

static void smh_soe_server_action(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task) {
    if (message->method == SOUP_METHOD_GET) {
        SMHPBEDB *db = smh_pbe_db_shared();

        g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
        g_autoptr(GArray) actions = smh__pbedb__ref_actions(SMH_PBE_DB_OBJECT(db));
        GE_ARRAY_FILTER(actions, i, e, SMH__PBEDBAction *, (e->type == SMH__PBEDBAUTOMATION__TYPE__TYPE_SCRIPT));
        smh_jse_builder_actions(builder, SMH_PBE_DB_OBJECT(db), actions, TRUE);
        g_autoptr(JsonNode) root = json_builder_get_root(JSON_BUILDER(builder));

        g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", root, NULL);
        gsize length = 0;
        gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

        soup_message_set_status(message, SOUP_STATUS_OK);
        soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    } else if (message->method == SOUP_METHOD_POST) {
        SMHPBEDB *db = smh_pbe_db_shared();

        GArray *actions = smh__pbedb__get_actions(SMH_PBE_DB_OBJECT(db));

        SMH__PBEDBAction *action = smh__pbedbaction__new();
        action->type = SMH__PBEDBAUTOMATION__TYPE__TYPE_SCRIPT;
        action->id = ++SMH_PBE_DB_OBJECT(db)->action_id;

        g_autoptr(SMHJSEParser) parser = g_object_new(SMH_TYPE_JSE_PARSER, NULL);
        (void)json_parser_load_from_data(JSON_PARSER(parser), message->request_body->data, message->request_body->length, NULL);
        JsonNode *request_root = json_parser_get_root(JSON_PARSER(parser));

        g_autoptr(SMHJSEReader) reader = g_object_new(SMH_TYPE_JSE_READER, "root", request_root, NULL);
        smh_jse_reader_action(reader, SMH_PBE_DB_OBJECT(db), action);

        g_array_append_val(actions, action);

        smh__pbedb__set_actions(SMH_PBE_DB_OBJECT(db), actions);

        (void)pbe_db_write(PBE_DB(db), NULL);

        g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
        smh_jse_builder_action(builder, SMH_PBE_DB_OBJECT(db), action, FALSE);
        g_autoptr(JsonNode) response_root = json_builder_get_root(JSON_BUILDER(builder));

        g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", response_root, NULL);
        gsize length = 0;
        gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

        soup_message_set_status(message, SOUP_STATUS_OK);
        soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    } else if (message->method == SOUP_METHOD_PUT) {
        SMHPBEDB *db = smh_pbe_db_shared();

        g_autoptr(GArray) actions = smh__pbedb__ref_actions(SMH_PBE_DB_OBJECT(db));

        gchar *id = g_hash_table_lookup(query, "act_id");
        GE_ARRAY_FILTER(actions, i, e, SMH__PBEDBAction *, (e->id == atoi(id)));
        if (actions->len > 0) {
            SMH__PBEDBAction *action = g_array_index(actions, SMH__PBEDBAction *, 0);

            g_autoptr(SMHJSEParser) parser = g_object_new(SMH_TYPE_JSE_PARSER, NULL);
            (void)json_parser_load_from_data(JSON_PARSER(parser), message->request_body->data, message->request_body->length, NULL);
            JsonNode *request_root = json_parser_get_root(JSON_PARSER(parser));

            g_autoptr(SMHJSEReader) reader = g_object_new(SMH_TYPE_JSE_READER, "root", request_root, NULL);
            smh_jse_reader_action(reader, SMH_PBE_DB_OBJECT(db), action);

            (void)pbe_db_write(PBE_DB(db), NULL);

            g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
            smh_jse_builder_action(builder, SMH_PBE_DB_OBJECT(db), action, FALSE);
            g_autoptr(JsonNode) response_root = json_builder_get_root(JSON_BUILDER(builder));

            g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", response_root, NULL);
            gsize length = 0;
            gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

            soup_message_set_status(message, SOUP_STATUS_OK);
            soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
        }
    } else if (message->method == SOUP_METHOD_DELETE) {
        SMHPBEDB *db = smh_pbe_db_shared();

        GArray *actions = smh__pbedb__get_actions(SMH_PBE_DB_OBJECT(db));

        gchar *id = g_hash_table_lookup(query, "act_id");
        GE_ARRAY_FILTER(actions, i, e, SMH__PBEDBAction *, (e->id != atoi(id)));

        smh__pbedb__set_actions(SMH_PBE_DB_OBJECT(db), actions);

        (void)pbe_db_write(PBE_DB(db), NULL);

        soup_message_set_status(message, SOUP_STATUS_OK);
    } else {
        soup_message_set_status(message, SOUP_STATUS_NOT_IMPLEMENTED);
    }

    g_print("%s %s\n", message->method, path);
}

static void smh_soe_server_condition_sensor(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task) {
    if (message->method == SOUP_METHOD_GET) {
        SMHPBEDB *db = smh_pbe_db_shared();

        g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
        g_autoptr(GArray) conditions = smh__pbedb__ref_conditions(SMH_PBE_DB_OBJECT(db));
        GE_ARRAY_FILTER(conditions, i, e, SMH__PBEDBCondition *, (e->type == SMH__PBEDBAUTOMATION__TYPE__TYPE_SCRIPT));
        smh_jse_builder_condition_sensors(builder, SMH_PBE_DB_OBJECT(db), conditions, TRUE);
        g_autoptr(JsonNode) root = json_builder_get_root(JSON_BUILDER(builder));

        g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", root, NULL);
        gsize length = 0;
        gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

        soup_message_set_status(message, SOUP_STATUS_OK);
        soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    } else if (message->method == SOUP_METHOD_POST) {
        SMHPBEDB *db = smh_pbe_db_shared();

        GArray *conditions = smh__pbedb__get_conditions(SMH_PBE_DB_OBJECT(db));

        SMH__PBEDBCondition *condition = smh__pbedbcondition__new();
        condition->type = SMH__PBEDBAUTOMATION__TYPE__TYPE_SCRIPT;
        condition->id = ++SMH_PBE_DB_OBJECT(db)->condition_id;

        g_autoptr(SMHJSEParser) parser = g_object_new(SMH_TYPE_JSE_PARSER, NULL);
        (void)json_parser_load_from_data(JSON_PARSER(parser), message->request_body->data, message->request_body->length, NULL);
        JsonNode *request_root = json_parser_get_root(JSON_PARSER(parser));

        g_autoptr(SMHJSEReader) reader = g_object_new(SMH_TYPE_JSE_READER, "root", request_root, NULL);
        smh_jse_reader_condition_sensor(reader, SMH_PBE_DB_OBJECT(db), condition);

        g_array_append_val(conditions, condition);

        smh__pbedb__set_conditions(SMH_PBE_DB_OBJECT(db), conditions);

        (void)pbe_db_write(PBE_DB(db), NULL);

        g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
        smh_jse_builder_condition_sensor(builder, SMH_PBE_DB_OBJECT(db), condition, FALSE);
        g_autoptr(JsonNode) response_root = json_builder_get_root(JSON_BUILDER(builder));

        g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", response_root, NULL);
        gsize length = 0;
        gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

        soup_message_set_status(message, SOUP_STATUS_OK);
        soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    } else if (message->method == SOUP_METHOD_PUT) {
        SMHPBEDB *db = smh_pbe_db_shared();

        g_autoptr(GArray) conditions = smh__pbedb__ref_conditions(SMH_PBE_DB_OBJECT(db));

        gchar *id = g_hash_table_lookup(query, "cnsn_id");
        GE_ARRAY_FILTER(conditions, i, e, SMH__PBEDBCondition *, (e->id == atoi(id)));
        if (conditions->len > 0) {
            SMH__PBEDBCondition *condition = g_array_index(conditions, SMH__PBEDBCondition *, 0);

            g_autoptr(SMHJSEParser) parser = g_object_new(SMH_TYPE_JSE_PARSER, NULL);
            (void)json_parser_load_from_data(JSON_PARSER(parser), message->request_body->data, message->request_body->length, NULL);
            JsonNode *request_root = json_parser_get_root(JSON_PARSER(parser));

            g_autoptr(SMHJSEReader) reader = g_object_new(SMH_TYPE_JSE_READER, "root", request_root, NULL);
            smh_jse_reader_condition_sensor(reader, SMH_PBE_DB_OBJECT(db), condition);

            (void)pbe_db_write(PBE_DB(db), NULL);

            g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
            smh_jse_builder_condition_sensor(builder, SMH_PBE_DB_OBJECT(db), condition, FALSE);
            g_autoptr(JsonNode) response_root = json_builder_get_root(JSON_BUILDER(builder));

            g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", response_root, NULL);
            gsize length = 0;
            gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

            soup_message_set_status(message, SOUP_STATUS_OK);
            soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
        }
    } else if (message->method == SOUP_METHOD_DELETE) {
        SMHPBEDB *db = smh_pbe_db_shared();

        GArray *conditions = smh__pbedb__get_conditions(SMH_PBE_DB_OBJECT(db));

        gchar *id = g_hash_table_lookup(query, "cnsn_id");
        GE_ARRAY_FILTER(conditions, i, e, SMH__PBEDBCondition *, (e->id != atoi(id)));

        smh__pbedb__set_conditions(SMH_PBE_DB_OBJECT(db), conditions);

        (void)pbe_db_write(PBE_DB(db), NULL);

        soup_message_set_status(message, SOUP_STATUS_OK);
    } else {
        soup_message_set_status(message, SOUP_STATUS_NOT_IMPLEMENTED);
    }

    g_print("%s %s\n", message->method, path);
}

static void smh_soe_server_used_devices(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task) {
    if (message->method == SOUP_METHOD_GET) {
        SMHPBEDB *db = smh_pbe_db_shared();

        gchar *device = g_hash_table_lookup(query, "dev_id");
        gchar *group = g_hash_table_lookup(query, "group_id");

        gboolean result = FALSE;
        if (device != NULL) {
            g_autoptr(GArray) conditions = smh__pbedb__ref_conditions(SMH_PBE_DB_OBJECT(db));
            GE_ARRAY_FILTER(conditions, i, e, SMH__PBEDBCondition *, (e->peripheral == atoi(device)));
            if (conditions->len > 0) {
                result = TRUE;
            } else {
                g_autoptr(GArray) actions = smh__pbedb__ref_actions(SMH_PBE_DB_OBJECT(db));
                GE_ARRAY_FILTER(actions, i, e, SMH__PBEDBAction *, ((e->type == SMH__PBEDBAUTOMATION__TYPE__TYPE_SCRIPT) && (e->n_devices > 0)));
                for (guint index = 0; index < actions->len; index++) {
                    SMH__PBEDBAction *action = g_array_index(actions, SMH__PBEDBAction *, index);
                    g_autoptr(GArray) devices = smh__pbedbaction__ref_devices(action);
                    GE_ARRAY_FILTER(devices, i, e, guint32, (e == atoi(device)));
                    if (devices->len > 0) {
                        result = TRUE;
                        break;
                    }
                }
            }
        } else {
            g_autoptr(GArray) actions = smh__pbedb__ref_actions(SMH_PBE_DB_OBJECT(db));
            GE_ARRAY_FILTER(actions, i, e, SMH__PBEDBAction *, ((e->type == SMH__PBEDBAUTOMATION__TYPE__TYPE_SCRIPT) && (e->n_groups > 0)));
            for (guint index = 0; index < actions->len; index++) {
                SMH__PBEDBAction *action = g_array_index(actions, SMH__PBEDBAction *, index);
                g_autoptr(GArray) groups = smh__pbedbaction__ref_groups(action);
                GE_ARRAY_FILTER(groups, i, e, guint32, (e == atoi(group)));
                if (groups->len > 0) {
                    result = TRUE;
                    break;
                }
            }
        }

        g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
        smh_jse_builder_used(builder, result);
        g_autoptr(JsonNode) root = json_builder_get_root(JSON_BUILDER(builder));

        g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", root, NULL);
        gsize length = 0;
        gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

        soup_message_set_status(message, SOUP_STATUS_OK);
        soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    } else {
        soup_message_set_status(message, SOUP_STATUS_NOT_IMPLEMENTED);
    }

    g_print("%s %s\n", message->method, path);
}

static void smh_soe_server_used_recipients(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task) {
    if (message->method == SOUP_METHOD_GET) {
        SMHPBEDB *db = smh_pbe_db_shared();

        gchar *push = g_hash_table_lookup(query, "mobile_id");
        gchar *sms = g_hash_table_lookup(query, "phone_id");
        gchar *id = (push != NULL) ? push : sms;

        gboolean result = FALSE;
        g_autoptr(GArray) actions = smh__pbedb__ref_actions(SMH_PBE_DB_OBJECT(db));
        GE_ARRAY_FILTER(actions, i, e, SMH__PBEDBAction *, ((e->cluster == SMH__PBEDBCLUSTER__ID__ID_NOTIFICATION) && ((e->parameters[0] == SMH__PBEDBACTION__NOTIFICATION__NOTIFICATION_PUSH) || (e->parameters[0] == SMH__PBEDBACTION__NOTIFICATION__NOTIFICATION_SMS))));
        for (guint index = 0; index < actions->len; index++) {
            SMH__PBEDBAction *action = g_array_index(actions, SMH__PBEDBAction *, index);
            g_autoptr(GArray) subscribers = smh__pbedbaction__ref_parameters(action);
            g_array_remove_index(subscribers, 0);
            GE_ARRAY_FILTER(subscribers, i, e, gint32, (e == atoi(id)));
            if (subscribers->len > 0) {
                result = TRUE;
                break;
            }
        }
        
        g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
        smh_jse_builder_used(builder, result);
        g_autoptr(JsonNode) root = json_builder_get_root(JSON_BUILDER(builder));

        g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", root, NULL);
        gsize length = 0;
        gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

        soup_message_set_status(message, SOUP_STATUS_OK);
        soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    } else {
        soup_message_set_status(message, SOUP_STATUS_NOT_IMPLEMENTED);
    }

    g_print("%s %s\n", message->method, path);
}

static void smh_soe_server_mode(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task) {
    if (message->method == SOUP_METHOD_GET) {
        SMHPBEDB *db = smh_pbe_db_shared();

        g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
        smh_jse_builder_get_mode(builder, SMH_PBE_DB_OBJECT(db), TRUE);
        g_autoptr(JsonNode) root = json_builder_get_root(JSON_BUILDER(builder));

        g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", root, NULL);
        gsize length = 0;
        gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

        soup_message_set_status(message, SOUP_STATUS_OK);
        soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    } else if (message->method == SOUP_METHOD_POST) {
        SMHPBEDB *db = smh_pbe_db_shared();

        GArray *automations = smh__pbedb__get_automations(SMH_PBE_DB_OBJECT(db));

        SMH__PBEDBAutomation *mode = smh__pbedbautomation__new();
        mode->type = SMH__PBEDBAUTOMATION__TYPE__TYPE_MODE;
        mode->id = ++SMH_PBE_DB_OBJECT(db)->automation_id;

        g_autoptr(SMHJSEParser) parser = g_object_new(SMH_TYPE_JSE_PARSER, NULL);
        (void)json_parser_load_from_data(JSON_PARSER(parser), message->request_body->data, message->request_body->length, NULL);
        JsonNode *request_root = json_parser_get_root(JSON_PARSER(parser));

        g_autoptr(SMHJSEReader) reader = g_object_new(SMH_TYPE_JSE_READER, "root", request_root, NULL);
        smh_jse_reader_mode(reader, SMH_PBE_DB_OBJECT(db), mode);

        g_array_append_val(automations, mode);

        smh__pbedb__set_automations(SMH_PBE_DB_OBJECT(db), automations);

        (void)pbe_db_write(PBE_DB(db), NULL);

        g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
        smh_jse_builder_mode(builder, SMH_PBE_DB_OBJECT(db), mode, FALSE);
        g_autoptr(JsonNode) response_root = json_builder_get_root(JSON_BUILDER(builder));

        g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", response_root, NULL);
        gsize length = 0;
        gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

        soup_message_set_status(message, SOUP_STATUS_OK);
        soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    } else if (message->method == SOUP_METHOD_PUT) {
        SMHPBEDB *db = smh_pbe_db_shared();

        g_autoptr(GArray) automations = smh__pbedb__ref_automations(SMH_PBE_DB_OBJECT(db));

        gchar *id = g_hash_table_lookup(query, "mode_id");
        GE_ARRAY_FILTER(automations, i, e, SMH__PBEDBAutomation *, (e->id == atoi(id)));
        if (automations->len > 0) {
            SMH__PBEDBAutomation *mode = g_array_index(automations, SMH__PBEDBAutomation *, 0);

            g_autoptr(SMHJSEParser) parser = g_object_new(SMH_TYPE_JSE_PARSER, NULL);
            (void)json_parser_load_from_data(JSON_PARSER(parser), message->request_body->data, message->request_body->length, NULL);
            JsonNode *request_root = json_parser_get_root(JSON_PARSER(parser));

            g_autoptr(SMHJSEReader) reader = g_object_new(SMH_TYPE_JSE_READER, "root", request_root, NULL);
            smh_jse_reader_mode(reader, SMH_PBE_DB_OBJECT(db), mode);

            (void)pbe_db_write(PBE_DB(db), NULL);

            g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
            smh_jse_builder_mode(builder, SMH_PBE_DB_OBJECT(db), mode, FALSE);
            g_autoptr(JsonNode) response_root = json_builder_get_root(JSON_BUILDER(builder));

            g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", response_root, NULL);
            gsize length = 0;
            gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

            soup_message_set_status(message, SOUP_STATUS_OK);
            soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
        }
    } else if (message->method == SOUP_METHOD_DELETE) {
        SMHPBEDB *db = smh_pbe_db_shared();

        GArray *automations = smh__pbedb__get_automations(SMH_PBE_DB_OBJECT(db));
        GArray *schedulers = smh__pbedb__get_schedulers(SMH_PBE_DB_OBJECT(db));
        GArray *actions = smh__pbedb__get_actions(SMH_PBE_DB_OBJECT(db));
        GArray *conditions = smh__pbedb__get_conditions(SMH_PBE_DB_OBJECT(db));

        gchar *id = g_hash_table_lookup(query, "mode_id");
        GE_ARRAY_FILTER(automations, i, e, SMH__PBEDBAutomation *, (e->id != atoi(id)));
        GE_ARRAY_FILTER(schedulers, i, e, SMH__PBEDBScheduler *, (e->automation != atoi(id)));
        GE_ARRAY_FILTER(actions, i, e, SMH__PBEDBAction *, (e->automation != atoi(id)));
        GE_ARRAY_FILTER(conditions, i, e, SMH__PBEDBCondition *, (e->automation != atoi(id)));

        smh__pbedb__set_automations(SMH_PBE_DB_OBJECT(db), automations);
        smh__pbedb__set_schedulers(SMH_PBE_DB_OBJECT(db), schedulers);
        smh__pbedb__set_actions(SMH_PBE_DB_OBJECT(db), actions);
        smh__pbedb__set_conditions(SMH_PBE_DB_OBJECT(db), conditions);

        (void)pbe_db_write(PBE_DB(db), NULL);

        soup_message_set_status(message, SOUP_STATUS_OK);
    } else {
        soup_message_set_status(message, SOUP_STATUS_NOT_IMPLEMENTED);
    }

    g_print("%s %s\n", message->method, path);
}

static void smh_soe_server_run_mode(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task) {
    if (message->method == SOUP_METHOD_POST) {
        SMHPBEDB *db = smh_pbe_db_shared();

        g_autoptr(GArray) automations = smh__pbedb__ref_automations(SMH_PBE_DB_OBJECT(db));

        gchar *id = g_hash_table_lookup(query, "mode_id");
        GE_ARRAY_FILTER(automations, i, e, SMH__PBEDBAutomation *, (e->id == atoi(id)));
        if (automations->len > 0) {
            SMH__PBEDBAutomation *mode = g_array_index(automations, SMH__PBEDBAutomation *, 0);
            mode->on = g_str_has_suffix(path, "1");
            (void)pbe_db_write(PBE_DB(db), NULL);

            SMHManager *manager = smh_manager_shared();
            smh_manager_automation_set_on(manager, SMH_PBE_DB_OBJECT(db), mode);
        }

        soup_message_set_status(message, SOUP_STATUS_OK);
    } else {
        soup_message_set_status(message, SOUP_STATUS_NOT_IMPLEMENTED);
    }

    g_print("%s %s\n", message->method, path);
}

static void smh_soe_server_configure_button(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task) {
    if (message->method == SOUP_METHOD_GET) {
        SMHPBEDB *db = smh_pbe_db_shared();

        g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
        g_autoptr(GArray) devices = smh__pbedb__ref_peripherals(SMH_PBE_DB_OBJECT(db));
        GE_ARRAY_FILTER(devices, i, e, SMH__PBEDBPeripheral *, (e->n_bindings > 0));
        smh_jse_builder_configure_buttons(builder, SMH_PBE_DB_OBJECT(db), devices, TRUE);
        g_autoptr(JsonNode) root = json_builder_get_root(JSON_BUILDER(builder));

        g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", root, NULL);
        gsize length = 0;
        gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

        soup_message_set_status(message, SOUP_STATUS_OK);
        soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    } else if (message->method == SOUP_METHOD_POST) {
        SMHPBEDB *db = smh_pbe_db_shared();

        g_autoptr(SMHJSEParser) parser = g_object_new(SMH_TYPE_JSE_PARSER, NULL);
        (void)json_parser_load_from_data(JSON_PARSER(parser), message->request_body->data, message->request_body->length, NULL);
        JsonNode *root = json_parser_get_root(JSON_PARSER(parser));

        g_autoptr(SMHJSEReader) reader = g_object_new(SMH_TYPE_JSE_READER, "root", root, NULL);
        smh_jse_reader_configure_button(reader, SMH_PBE_DB_OBJECT(db));

        (void)pbe_db_write(PBE_DB(db), NULL);

        soup_message_set_status(message, SOUP_STATUS_OK);
    } else {
        soup_message_set_status(message, SOUP_STATUS_NOT_IMPLEMENTED);
    }

    g_print("%s %s\n", message->method, path);
}

static void smh_soe_server_mobile_registration(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task) {
    if (message->method == SOUP_METHOD_GET) {
        SMHPBEDB *db = smh_pbe_db_shared();

        g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
        g_autoptr(GArray) subscribers = smh__pbedb__ref_subscribers(SMH_PBE_DB_OBJECT(db));
        GE_ARRAY_FILTER(subscribers, i, e, SMH__PBEDBSubscriber *, (e->type == SMH__PBEDBSUBSCRIBER__TYPE__TYPE_PUSH));
        smh_jse_builder_mobile_registrations(builder, SMH_PBE_DB_OBJECT(db), subscribers, TRUE);
        g_autoptr(JsonNode) root = json_builder_get_root(JSON_BUILDER(builder));

        g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", root, NULL);
        gsize length = 0;
        gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

        soup_message_set_status(message, SOUP_STATUS_OK);
        soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    } else if (message->method == SOUP_METHOD_POST) {
        SMHPBEDB *db = smh_pbe_db_shared();

        GArray *subscribers = smh__pbedb__get_subscribers(SMH_PBE_DB_OBJECT(db));

        SMH__PBEDBSubscriber *subscriber = smh__pbedbsubscriber__new();
        subscriber->type = SMH__PBEDBSUBSCRIBER__TYPE__TYPE_PUSH;
        subscriber->id = ++SMH_PBE_DB_OBJECT(db)->subscriber_id;

        g_autoptr(SMHJSEParser) parser = g_object_new(SMH_TYPE_JSE_PARSER, NULL);
        (void)json_parser_load_from_data(JSON_PARSER(parser), message->request_body->data, message->request_body->length, NULL);
        JsonNode *request_root = json_parser_get_root(JSON_PARSER(parser));

        g_autoptr(SMHJSEReader) reader = g_object_new(SMH_TYPE_JSE_READER, "root", request_root, NULL);
        smh_jse_reader_mobile_registration(reader, SMH_PBE_DB_OBJECT(db), subscriber);

        g_array_append_val(subscribers, subscriber);

        smh__pbedb__set_subscribers(SMH_PBE_DB_OBJECT(db), subscribers);

        (void)pbe_db_write(PBE_DB(db), NULL);

        g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
        smh_jse_builder_mobile_registration(builder, SMH_PBE_DB_OBJECT(db), subscriber, FALSE);
        g_autoptr(JsonNode) response_root = json_builder_get_root(JSON_BUILDER(builder));

        g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", response_root, NULL);
        gsize length = 0;
        gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

        soup_message_set_status(message, SOUP_STATUS_OK);
        soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    } else if (message->method == SOUP_METHOD_PUT) {
        SMHPBEDB *db = smh_pbe_db_shared();

        g_autoptr(GArray) subscribers = smh__pbedb__ref_subscribers(SMH_PBE_DB_OBJECT(db));

        gchar *id = g_hash_table_lookup(query, "mobile_id");
        GE_ARRAY_FILTER(subscribers, i, e, SMH__PBEDBSubscriber *, (e->id == atoi(id)));
        if (subscribers->len > 0) {
            SMH__PBEDBSubscriber *subscriber = g_array_index(subscribers, SMH__PBEDBSubscriber *, 0);

            g_autoptr(SMHJSEParser) parser = g_object_new(SMH_TYPE_JSE_PARSER, NULL);
            (void)json_parser_load_from_data(JSON_PARSER(parser), message->request_body->data, message->request_body->length, NULL);
            JsonNode *request_root = json_parser_get_root(JSON_PARSER(parser));

            g_autoptr(SMHJSEReader) reader = g_object_new(SMH_TYPE_JSE_READER, "root", request_root, NULL);
            smh_jse_reader_mobile_registration(reader, SMH_PBE_DB_OBJECT(db), subscriber);

            (void)pbe_db_write(PBE_DB(db), NULL);

            g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
            smh_jse_builder_mobile_registration(builder, SMH_PBE_DB_OBJECT(db), subscriber, FALSE);
            g_autoptr(JsonNode) response_root = json_builder_get_root(JSON_BUILDER(builder));

            g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", response_root, NULL);
            gsize length = 0;
            gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

            soup_message_set_status(message, SOUP_STATUS_OK);
            soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
        }
    } else if (message->method == SOUP_METHOD_DELETE) {
        SMHPBEDB *db = smh_pbe_db_shared();

        GArray *subscribers = smh__pbedb__get_subscribers(SMH_PBE_DB_OBJECT(db));

        gchar *id = g_hash_table_lookup(query, "mobile_id");
        GE_ARRAY_FILTER(subscribers, i, e, SMH__PBEDBSubscriber *, (e->id != atoi(id)));

        smh__pbedb__set_subscribers(SMH_PBE_DB_OBJECT(db), subscribers);

        (void)pbe_db_write(PBE_DB(db), NULL);

        soup_message_set_status(message, SOUP_STATUS_OK);
    } else {
        soup_message_set_status(message, SOUP_STATUS_NOT_IMPLEMENTED);
    }

    g_print("%s %s\n", message->method, path);
}

static void smh_soe_server_phone_number(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task) {
    if (message->method == SOUP_METHOD_GET) {
        SMHPBEDB *db = smh_pbe_db_shared();

        g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
        g_autoptr(GArray) subscribers = smh__pbedb__ref_subscribers(SMH_PBE_DB_OBJECT(db));
        GE_ARRAY_FILTER(subscribers, i, e, SMH__PBEDBSubscriber *, (e->type == SMH__PBEDBSUBSCRIBER__TYPE__TYPE_SMS));
        smh_jse_builder_phone_numbers(builder, SMH_PBE_DB_OBJECT(db), subscribers, TRUE);
        g_autoptr(JsonNode) root = json_builder_get_root(JSON_BUILDER(builder));

        g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", root, NULL);
        gsize length = 0;
        gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

        soup_message_set_status(message, SOUP_STATUS_OK);
        soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    } else {
        soup_message_set_status(message, SOUP_STATUS_NOT_IMPLEMENTED);
    }

    g_print("%s %s\n", message->method, path);
}

static void smh_soe_server_update_device(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task) {
    if (message->method == SOUP_METHOD_GET) {
        SMHPBEDB *db = smh_pbe_db_shared();

        g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
        g_autoptr(GArray) updates = smh__pbedb__ref_updates(SMH_PBE_DB_OBJECT(db));
        smh_jse_builder_update_devices(builder, SMH_PBE_DB_OBJECT(db), updates, TRUE);
        g_autoptr(JsonNode) root = json_builder_get_root(JSON_BUILDER(builder));

        g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", root, NULL);
        gsize length = 0;
        gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

        soup_message_set_status(message, SOUP_STATUS_OK);
        soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);
    } else {
        soup_message_set_status(message, SOUP_STATUS_NOT_IMPLEMENTED);
    }

    g_print("%s %s\n", message->method, path);
}

static void smh_soe_server_sync_status(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task) {
    if (message->method == SOUP_METHOD_GET) {
        g_autoptr(GArray) messages = smh_soe_server_get_messages(SMH_SOE_SERVER(self));

        soup_message_headers_set_encoding(message->response_headers, SOUP_ENCODING_CHUNKED);
        g_signal_connect(message, "finished", G_CALLBACK(smh_soe_server_soup_message_finished), self);
        g_array_append_val(messages, message);

        soup_message_headers_set_content_type(message->response_headers, JSE_MIME_TYPE, NULL);
        soup_message_set_status(message, SOUP_STATUS_OK);
    } else {
        soup_message_set_status(message, SOUP_STATUS_NOT_IMPLEMENTED);
    }

    g_print("%s %s\n", message->method, path);
}

static void smh_soe_server_default(SoupServer *self, SoupMessage *message, const gchar *path, GHashTable *query, SoupClientContext *client, gpointer task) {
    g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
    smh_jse_builder_default(builder);
    g_autoptr(JsonNode) root = json_builder_get_root(JSON_BUILDER(builder));

    g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", root, NULL);
    gsize length = 0;
    gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

    soup_message_set_status(message, SOUP_STATUS_BAD_REQUEST);
    soup_message_set_response(message, JSE_MIME_TYPE, SOUP_MEMORY_TAKE, body, length);

    g_print("%s %s\n", message->method, path);
}

static void smh_soe_server_soup_message_finished(SoupMessage *sender, gpointer self) {
    g_autoptr(GArray) messages = smh_soe_server_get_messages(SMH_SOE_SERVER(self));
    GE_ARRAY_FILTER(messages, i, e, SoupMessage *, (e != sender));
    g_print("finished\n");
}

static void smh_soe_server_smh_manager_peripheral_value_set(SMHManager *sender, SMH__PBEDB *db, SMH__PBEDBPeripheral *peripheral, SMH__PBEDBCluster *cluster, SMH__PBEDBAttribute *attribute, gpointer self) {
    SMH__PBEDBEvent event = SMH__PBEDBEVENT__INIT;
    event.type = (peripheral->type == SMH__PBEDBPERIPHERAL__TYPE__TYPE_DEVICE) ? SMH__PBEDBEVENT__TYPE__TYPE_DEVICE : SMH__PBEDBEVENT__TYPE__TYPE_GROUP;
    event.object_id = peripheral->id;
    event.cluster = cluster->id;
    event.attribute = attribute->key;
    event.str_value = attribute->str_value;
    event.num_value = 1;
    smh_soe_server_send_event(self, &event);
}

static void smh_soe_server_smh_manager_automation_on_set(SMHManager *sender, SMH__PBEDB *db, SMH__PBEDBAutomation *automation, gpointer self) {
    SMH__PBEDBEvent event = SMH__PBEDBEVENT__INIT;
    event.type = (automation->type == SMH__PBEDBAUTOMATION__TYPE__TYPE_SCRIPT) ? SMH__PBEDBEVENT__TYPE__TYPE_SCRIPT : SMH__PBEDBEVENT__TYPE__TYPE_MODE;
    event.object_id = automation->id;
    event.str_value = (automation->on == TRUE) ? "1" : "0";
    event.num_value = 1;
    smh_soe_server_send_event(self, &event);
}

static void smh_soe_server_send_event(SMHSOEServer *self, SMH__PBEDBEvent *event) {
    g_autoptr(GArray) messages = smh_soe_server_get_messages(SMH_SOE_SERVER(self));

    for (guint index = 0; index < messages->len; index++) {
        SoupMessage *message = g_array_index(messages, SoupMessage *, index);

        g_autoptr(SMHJSEBuilder) builder = g_object_new(SMH_TYPE_JSE_BUILDER, NULL);
        smh_jse_builder_event(builder, event);
        g_autoptr(JsonNode) root = json_builder_get_root(JSON_BUILDER(builder));

        g_autoptr(SMHJSEGenerator) generator = g_object_new(SMH_TYPE_JSE_GENERATOR, "root", root, NULL);
        gsize length = 0;
        gchar *body = json_generator_to_data(JSON_GENERATOR(generator), &length);

        soup_message_body_append(message->response_body, SOUP_MEMORY_TAKE, body, length);
        soup_message_body_append(message->response_body, SOUP_MEMORY_COPY, "\r\n", 2);
        soup_server_unpause_message(SOUP_SERVER(self), message);
    }
}

void smh_soe_server_restart(SMHSOEServer *self) {
    guint port = smh_soe_server_get_port(self);

    soup_server_disconnect(SOUP_SERVER(self));
    (void)soup_server_listen_all(SOUP_SERVER(self), port, 0, NULL);
}

SMHSOEServer *_smh_soe_server_shared(void) {
    SMHSOEServer *self = g_object_new(SMH_TYPE_SOE_SERVER, NULL);
    return self;
}
