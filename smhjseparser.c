//
// Created by dan on 28.08.2019.
//

#include "smhjseparser.h"

typedef struct {
    gpointer padding[1];
} SMHJSEParserPrivate;

G_DEFINE_TYPE_WITH_CODE(SMHJSEParser, smh_jse_parser, JSE_TYPE_PARSER, G_ADD_PRIVATE(SMHJSEParser))

static void smh_jse_parser_class_init(SMHJSEParserClass *class) {

}

static void smh_jse_parser_init(SMHJSEParser *self) {

}
