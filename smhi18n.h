//
// Created by root on 05.03.2020.
//

#ifndef LIBRARY_SMARTHOME_SMHI18N_H
#define LIBRARY_SMARTHOME_SMHI18N_H

#include "smhmain.h"

G_BEGIN_DECLS

#define GETTEXT_PACKAGE "library-smarthome"
#include <glib/gi18n-lib.h>

G_END_DECLS

#endif //LIBRARY_SMARTHOME_SMHI18N_H
