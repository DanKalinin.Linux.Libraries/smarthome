//
// Created by root on 05.03.2020.
//

#include "smhi18n.h"

G_DEFINE_CONSTRUCTOR(smh_i18n_constructor)

static void smh_i18n_constructor(void) {
    (void)bindtextdomain(GETTEXT_PACKAGE, NULL);
    (void)bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
}
