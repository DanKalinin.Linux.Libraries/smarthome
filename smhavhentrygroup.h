//
// Created by dan on 11.08.2019.
//

#ifndef LIBRARY_SMARTHOME_SMHAVHENTRYGROUP_H
#define LIBRARY_SMARTHOME_SMHAVHENTRYGROUP_H

#include "smhmain.h"
#include "smhavhclient.h"
#include "smhsoeserver.h"

G_BEGIN_DECLS

#define SMH_TYPE_AVH_ENTRY_GROUP smh_avh_entry_group_get_type()

G_DECLARE_DERIVABLE_TYPE(SMHAVHEntryGroup, smh_avh_entry_group, INT, AVH_ENTRY_GROUP, AVHEntryGroup)

struct _SMHAVHEntryGroupClass {
    AVHEntryGroupClass super;
};

GE_POINTER_SHARED(SMHAVHEntryGroup, smh_avh_entry_group)

G_END_DECLS

#endif //LIBRARY_SMARTHOME_SMHAVHENTRYGROUP_H
