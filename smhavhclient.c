//
// Created by dan on 11.08.2019.
//

#include "smhavhclient.h"

typedef struct {
    gpointer padding[1];
} SMHAVHClientPrivate;

G_DEFINE_TYPE_WITH_CODE(SMHAVHClient, smh_avh_client, AVH_TYPE_CLIENT, G_ADD_PRIVATE(SMHAVHClient))

GE_POINTER_SHARED_C(SMHAVHClient, smh_avh_client)

static void smh_avh_client_class_init(SMHAVHClientClass *class) {

}

static void smh_avh_client_init(SMHAVHClient *self) {
    AvahiGLibPoll *source = avahi_glib_poll_new(NULL, G_PRIORITY_LOW);
    const AvahiPoll *poll = avahi_glib_poll_get(source);

    AvahiClient *object = avahi_client_new(poll, 0, avh_client_state_callback, self, NULL);
    avh_client_set_object(AVH_CLIENT(self), object);
    avh_client_set_source(AVH_CLIENT(self), source);
}

SMHAVHClient *_smh_avh_client_shared(void) {
    SMHAVHClient *self = g_object_new(SMH_TYPE_AVH_CLIENT, NULL);
    return self;
}
