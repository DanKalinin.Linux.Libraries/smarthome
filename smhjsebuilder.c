//
// Created by dan on 23.08.2019.
//

#include "smhjsebuilder.h"
#include "smhi18n.h"

typedef struct {
    gpointer padding[1];
} SMHJSEBuilderPrivate;

G_DEFINE_TYPE_WITH_CODE(SMHJSEBuilder, smh_jse_builder, JSE_TYPE_BUILDER, G_ADD_PRIVATE(SMHJSEBuilder))

static void smh_jse_builder_class_init(SMHJSEBuilderClass *class) {

}

static void smh_jse_builder_init(SMHJSEBuilder *self) {

}

void smh_jse_builder_receiver_info(SMHJSEBuilder *self, SMH__PBEDB *db, gboolean full) {
    (void)json_builder_begin_object(JSON_BUILDER(self));
    (void)json_builder_set_member_name(JSON_BUILDER(self), "modelName");
    (void)json_builder_add_string_value(JSON_BUILDER(self), db->model_name);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "userFriendlyModelName");
    (void)json_builder_add_string_value(JSON_BUILDER(self), db->user_friendly_model_name);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "hardwareId");
    (void)json_builder_add_string_value(JSON_BUILDER(self), db->hardware_id);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "softwareVersion");
    (void)json_builder_add_string_value(JSON_BUILDER(self), db->software_version);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "serialNumber");
    (void)json_builder_add_string_value(JSON_BUILDER(self), db->serial_number);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "offset");
    (void)json_builder_add_int_value(JSON_BUILDER(self), db->offset);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "DREID");
    (void)json_builder_add_string_value(JSON_BUILDER(self), db->dre_id);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "apiversion");
    (void)json_builder_add_string_value(JSON_BUILDER(self), db->api_version);
    (void)json_builder_end_object(JSON_BUILDER(self));
}

void smh_jse_builder_dongle_version(SMHJSEBuilder *self, SMH__PBEDB *db, gboolean full) {
    (void)json_builder_begin_object(JSON_BUILDER(self));
    (void)json_builder_set_member_name(JSON_BUILDER(self), "version");
    (void)json_builder_add_string_value(JSON_BUILDER(self), db->version);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "hw_revision");
    (void)json_builder_add_string_value(JSON_BUILDER(self), db->hw_revision);
    (void)json_builder_end_object(JSON_BUILDER(self));
}

void smh_jse_builder_state(SMHJSEBuilder *self, SMH__PBEDB *db, gboolean full) {
    (void)json_builder_begin_object(JSON_BUILDER(self));
    (void)json_builder_set_member_name(JSON_BUILDER(self), "partition");
    (void)json_builder_add_int_value(JSON_BUILDER(self), (db->n_rooms == 0));
    (void)json_builder_set_member_name(JSON_BUILDER(self), "step");
    (void)json_builder_add_int_value(JSON_BUILDER(self), 0);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "active_client");
    (void)json_builder_add_string_value(JSON_BUILDER(self), db->active_client);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "session_id");
    (void)json_builder_add_string_value(JSON_BUILDER(self), db->session_id);
    (void)json_builder_end_object(JSON_BUILDER(self));
}

void smh_jse_builder_check_srls(SMHJSEBuilder *self, SMH__PBEDB *db, GArray *services, gboolean full) {
    (void)json_builder_begin_array(JSON_BUILDER(self));

    for (guint index = 0; index < services->len; index++) {
        SMH__PBEDBService *service = g_array_index(services, SMH__PBEDBService *, index);
        smh_jse_builder_check_srl(self, db, service, full);
    }

    (void)json_builder_end_array(JSON_BUILDER(self));
}

void smh_jse_builder_check_srl(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBService *service, gboolean full) {
    (void)json_builder_begin_object(JSON_BUILDER(self));
    (void)json_builder_set_member_name(JSON_BUILDER(self), "srls_id");
    (void)json_builder_add_int_value(JSON_BUILDER(self), service->id);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "srls_state");
    (void)json_builder_add_int_value(JSON_BUILDER(self), service->state);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "message");
    (void)json_builder_add_string_value(JSON_BUILDER(self), service->message);
    (void)json_builder_end_object(JSON_BUILDER(self));
}

void smh_jse_builder_get_configuration(SMHJSEBuilder *self, SMH__PBEDB *db, gboolean full) {
    (void)json_builder_begin_object(JSON_BUILDER(self));
    (void)json_builder_set_member_name(JSON_BUILDER(self), "rooms");
    g_autoptr(GArray) rooms = smh__pbedb__ref_rooms(db);
    smh_jse_builder_rooms(self, db, rooms, full);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "unknown_devices");
    g_autoptr(GArray) devices = smh__pbedb__ref_peripherals(db);
    GE_ARRAY_FILTER(devices, i, e, SMH__PBEDBPeripheral *, (e->undev == TRUE));
    smh_jse_builder_devices(self, db, devices, FALSE, full);
    (void)json_builder_end_object(JSON_BUILDER(self));
}

void smh_jse_builder_rooms(SMHJSEBuilder *self, SMH__PBEDB *db, GArray *rooms, gboolean full) {
    (void)json_builder_begin_array(JSON_BUILDER(self));

    for (guint index = 0; index < rooms->len; index++) {
        SMH__PBEDBRoom *room = g_array_index(rooms, SMH__PBEDBRoom *, index);
        smh_jse_builder_room(self, db, room, full);
    }

    (void)json_builder_end_array(JSON_BUILDER(self));
}

void smh_jse_builder_room(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBRoom *room, gboolean full) {
    (void)json_builder_begin_object(JSON_BUILDER(self));
    (void)json_builder_set_member_name(JSON_BUILDER(self), "room_id");
    (void)json_builder_add_int_value(JSON_BUILDER(self), room->id);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "room_name");
    (void)json_builder_add_string_value(JSON_BUILDER(self), room->name);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "rmlt_rmlt_id");
    (void)json_builder_add_int_value(JSON_BUILDER(self), room->type);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "room_foto");
    (void)json_builder_add_int_value(JSON_BUILDER(self), room->photo);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "foto_md5");
    (void)json_builder_add_string_value(JSON_BUILDER(self), room->photo_md5);

    if (full) {
        (void)json_builder_set_member_name(JSON_BUILDER(self), "groups");
        g_autoptr(GArray) groups = smh__pbedb__ref_peripherals(db);
        GE_ARRAY_FILTER(groups, i, e, SMH__PBEDBPeripheral *, ((e->type == SMH__PBEDBPERIPHERAL__TYPE__TYPE_GROUP) && (e->room == room->id)));
        smh_jse_builder_groupdevs(self, db, groups, full);
    }

    (void)json_builder_end_object(JSON_BUILDER(self));
}

void smh_jse_builder_groupdevs(SMHJSEBuilder *self, SMH__PBEDB *db, GArray *groups, gboolean full) {
    (void)json_builder_begin_array(JSON_BUILDER(self));

    for (guint index = 0; index < groups->len; index++) {
        SMH__PBEDBPeripheral *group = g_array_index(groups, SMH__PBEDBPeripheral *, index);
        smh_jse_builder_groupdev(self, db, group, full);
    }

    (void)json_builder_end_array(JSON_BUILDER(self));
}

void smh_jse_builder_groupdev(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBPeripheral *group, gboolean full) {
    (void)json_builder_begin_object(JSON_BUILDER(self));
    (void)json_builder_set_member_name(JSON_BUILDER(self), "group_id");
    (void)json_builder_add_int_value(JSON_BUILDER(self), group->id);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "group_name");
    (void)json_builder_add_string_value(JSON_BUILDER(self), group->name);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "group_x");
    (void)json_builder_add_int_value(JSON_BUILDER(self), group->x);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "group_y");
    (void)json_builder_add_int_value(JSON_BUILDER(self), group->y);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "group_z");
    (void)json_builder_add_int_value(JSON_BUILDER(self), group->z);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "room_room_id");
    (void)json_builder_add_int_value(JSON_BUILDER(self), group->room);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "grsn_grsn_id");
    (void)json_builder_add_int_value(JSON_BUILDER(self), group->group_sign);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "grtp_grtp_id");
    (void)json_builder_add_int_value(JSON_BUILDER(self), group->group_type);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "group_zcluster");
    g_autoptr(GArray) clusters = smh__pbedbperipheral__ref_clusters(group);
    smh_jse_builder_clusters(self, db, group, clusters, full);

    if (full) {
        (void)json_builder_set_member_name(JSON_BUILDER(self), "devices");
        g_autoptr(GArray) devices = smh__pbedb__ref_peripherals(db);
        GE_ARRAY_FILTER(devices, i, e, SMH__PBEDBPeripheral *, ((e->peripheral == group->id) && (e->undev == FALSE)));
        smh_jse_builder_devices(self, db, devices, FALSE, full);
    }

    (void)json_builder_end_object(JSON_BUILDER(self));
}

void smh_jse_builder_devices(SMHJSEBuilder *self, SMH__PBEDB *db, GArray *devices, gboolean output, gboolean full) {
    (void)json_builder_begin_array(JSON_BUILDER(self));

    for (guint index = 0; index < devices->len; index++) {
        SMH__PBEDBPeripheral *device = g_array_index(devices, SMH__PBEDBPeripheral *, index);
        smh_jse_builder_device(self, db, device, output, full);
    }

    (void)json_builder_end_array(JSON_BUILDER(self));
}

void smh_jse_builder_device(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBPeripheral *device, gboolean output, gboolean full) {
    (void)json_builder_begin_object(JSON_BUILDER(self));
    (void)json_builder_set_member_name(JSON_BUILDER(self), "dev_id");
    (void)json_builder_add_int_value(JSON_BUILDER(self), device->id);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "undev");
    (void)json_builder_add_int_value(JSON_BUILDER(self), device->undev);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "undev_address");
    (void)json_builder_add_int_value(JSON_BUILDER(self), device->undev_address);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "compozit_serial");
    (void)json_builder_add_int_value(JSON_BUILDER(self), device->composite_serial);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "dev_name");
    (void)json_builder_add_string_value(JSON_BUILDER(self), device->name);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "dvtp_num");
    (void)json_builder_add_int_value(JSON_BUILDER(self), device->device_type);

    if (device->undev) {
    } else {
        (void)json_builder_set_member_name(JSON_BUILDER(self), "group_group_id");
        (void)json_builder_add_int_value(JSON_BUILDER(self), device->peripheral);
        (void)json_builder_set_member_name(JSON_BUILDER(self), "ctgr_ctgr_id");
        (void)json_builder_add_int_value(JSON_BUILDER(self), device->device_category);
        (void)json_builder_set_member_name(JSON_BUILDER(self), "dvsn_dvsn_id");
        (void)json_builder_add_int_value(JSON_BUILDER(self), device->device_sign);
        (void)json_builder_set_member_name(JSON_BUILDER(self), "dev_x");
        (void)json_builder_add_int_value(JSON_BUILDER(self), device->x);
        (void)json_builder_set_member_name(JSON_BUILDER(self), "dev_y");
        (void)json_builder_add_int_value(JSON_BUILDER(self), device->y);
        (void)json_builder_set_member_name(JSON_BUILDER(self), "dev_z");
        (void)json_builder_add_int_value(JSON_BUILDER(self), device->z);
    }

    (void)json_builder_set_member_name(JSON_BUILDER(self), "zcluster");
    g_autoptr(GArray) clusters = smh__pbedbperipheral__ref_clusters(device);
    GE_ARRAY_FILTER(clusters, i, e, SMH__PBEDBCluster *, (e->output == output));
    smh_jse_builder_clusters(self, db, device, clusters, full);
    (void)json_builder_end_object(JSON_BUILDER(self));
}

void smh_jse_builder_clusters(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBPeripheral *peripheral, GArray *clusters, gboolean full) {
    (void)json_builder_begin_array(JSON_BUILDER(self));

    for (guint index = 0; index < clusters->len; index++) {
        SMH__PBEDBCluster *cluster = g_array_index(clusters, SMH__PBEDBCluster *, index);
        smh_jse_builder_cluster(self, db, peripheral, cluster, full);
    }

    (void)json_builder_end_array(JSON_BUILDER(self));
}

void smh_jse_builder_cluster(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBPeripheral *peripheral, SMH__PBEDBCluster *cluster, gboolean full) {
    (void)json_builder_begin_object(JSON_BUILDER(self));
    (void)json_builder_set_member_name(JSON_BUILDER(self), "zcl_id");
    (void)json_builder_add_int_value(JSON_BUILDER(self), cluster->id);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "attributes");
    g_autoptr(GArray) attributes = smh__pbedbcluster__ref_attributes(cluster);
    smh_jse_builder_attributes(self, db, peripheral, cluster, attributes, full);
    (void)json_builder_end_object(JSON_BUILDER(self));
}

void smh_jse_builder_attributes(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBPeripheral *peripheral, SMH__PBEDBCluster *cluster, GArray *attributes, gboolean full) {
    (void)json_builder_begin_array(JSON_BUILDER(self));

    for (guint index = 0; index < attributes->len; index++) {
        SMH__PBEDBAttribute *attribute = g_array_index(attributes, SMH__PBEDBAttribute *, index);
        smh_jse_builder_attribute(self, db, peripheral, cluster, attribute, full);
    }

    (void)json_builder_end_array(JSON_BUILDER(self));
}

void smh_jse_builder_attribute(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBPeripheral *peripheral, SMH__PBEDBCluster *cluster, SMH__PBEDBAttribute *attribute, gboolean full) {
    (void)json_builder_begin_object(JSON_BUILDER(self));
    (void)json_builder_set_member_name(JSON_BUILDER(self), "attr_key");
    (void)json_builder_add_int_value(JSON_BUILDER(self), attribute->key);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "str_attr_value");
    (void)json_builder_add_string_value(JSON_BUILDER(self), attribute->str_value);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "num_attr_value");
    (void)json_builder_add_int_value(JSON_BUILDER(self), attribute->num_value);
    (void)json_builder_end_object(JSON_BUILDER(self));
}

void smh_jse_builder_get_script(SMHJSEBuilder *self, SMH__PBEDB *db, gboolean full) {
    (void)json_builder_begin_object(JSON_BUILDER(self));
    (void)json_builder_set_member_name(JSON_BUILDER(self), "scr");
    g_autoptr(GArray) scripts = smh__pbedb__ref_automations(db);
    GE_ARRAY_FILTER(scripts, i, e, SMH__PBEDBAutomation *, (e->type == SMH__PBEDBAUTOMATION__TYPE__TYPE_SCRIPT));
    smh_jse_builder_scripts(self, db, scripts, full);
    (void)json_builder_end_object(JSON_BUILDER(self));
}

void smh_jse_builder_scripts(SMHJSEBuilder *self, SMH__PBEDB *db, GArray *scripts, gboolean full) {
    (void)json_builder_begin_array(JSON_BUILDER(self));

    for (guint index = 0; index < scripts->len; index++) {
        SMH__PBEDBAutomation *script = g_array_index(scripts, SMH__PBEDBAutomation *, index);
        smh_jse_builder_script(self, db, script, full);
    }

    (void)json_builder_end_array(JSON_BUILDER(self));
}

void smh_jse_builder_script(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBAutomation *script, gboolean full) {
    (void)json_builder_begin_object(JSON_BUILDER(self));
    (void)json_builder_set_member_name(JSON_BUILDER(self), "scrt_id");
    (void)json_builder_add_int_value(JSON_BUILDER(self), script->id);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "scrt_name");
    (void)json_builder_add_string_value(JSON_BUILDER(self), script->name);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "script_activate");
    (void)json_builder_add_int_value(JSON_BUILDER(self), script->on);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "script_favorit");
    (void)json_builder_add_int_value(JSON_BUILDER(self), script->script_favorite);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "temp_temp_id");
    (void)json_builder_add_int_value(JSON_BUILDER(self), script->script_template);

    (void)json_builder_set_member_name(JSON_BUILDER(self), "schedulers");
    g_autoptr(GArray) schedulers = smh__pbedb__ref_schedulers(db);
    GE_ARRAY_FILTER(schedulers, i, e, SMH__PBEDBScheduler *, (e->automation == script->id));
    smh_jse_builder_schedulers(self, db, schedulers, full);

    (void)json_builder_set_member_name(JSON_BUILDER(self), "actions");
    g_autoptr(GArray) actions = smh__pbedb__ref_actions(db);
    GE_ARRAY_FILTER(actions, i, e, SMH__PBEDBAction *, (e->automation == script->id));
    smh_jse_builder_actions(self, db, actions, full);

    (void)json_builder_set_member_name(JSON_BUILDER(self), "condition_sensors");
    g_autoptr(GArray) conditions = smh__pbedb__ref_conditions(db);
    GE_ARRAY_FILTER(conditions, i, e, SMH__PBEDBCondition *, (e->automation == script->id));
    smh_jse_builder_condition_sensors(self, db, conditions, full);

    (void)json_builder_end_object(JSON_BUILDER(self));
}

void smh_jse_builder_schedulers(SMHJSEBuilder *self, SMH__PBEDB *db, GArray *schedulers, gboolean full) {
    (void)json_builder_begin_array(JSON_BUILDER(self));

    for (guint index = 0; index < schedulers->len; index++) {
        SMH__PBEDBScheduler *scheduler = g_array_index(schedulers, SMH__PBEDBScheduler *, index);
        smh_jse_builder_scheduler(self, db, scheduler, full);
    }

    (void)json_builder_end_array(JSON_BUILDER(self));
}

void smh_jse_builder_scheduler(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBScheduler *scheduler, gboolean full) {
    (void)json_builder_begin_object(JSON_BUILDER(self));
    (void)json_builder_set_member_name(JSON_BUILDER(self), "cond_id");
    (void)json_builder_add_int_value(JSON_BUILDER(self), scheduler->id);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "scrt_scrt_id");
    (void)json_builder_add_int_value(JSON_BUILDER(self), scheduler->automation);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "start_time");
    (void)json_builder_add_string_value(JSON_BUILDER(self), scheduler->start_time);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "end_time");
    (void)json_builder_add_string_value(JSON_BUILDER(self), scheduler->end_time);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "repeat");
    (void)json_builder_add_int_value(JSON_BUILDER(self), scheduler->repeat);

    // Weekdays

    (void)json_builder_set_member_name(JSON_BUILDER(self), "weekdays");

    (void)json_builder_begin_array(JSON_BUILDER(self));

    for (gsize index = 0; index < scheduler->n_weekdays; index++) {
        SMH__PBEDBScheduler__Weekday weekday = scheduler->weekdays[index];
        json_builder_add_int_value(JSON_BUILDER(self), weekday);
    }

    (void)json_builder_end_array(JSON_BUILDER(self));

    (void)json_builder_end_object(JSON_BUILDER(self));
}

void smh_jse_builder_actions(SMHJSEBuilder *self, SMH__PBEDB *db, GArray *actions, gboolean full) {
    (void)json_builder_begin_array(JSON_BUILDER(self));

    for (guint index = 0; index < actions->len; index++) {
        SMH__PBEDBAction *action = g_array_index(actions, SMH__PBEDBAction *, index);
        smh_jse_builder_action(self, db, action, full);
    }

    (void)json_builder_end_array(JSON_BUILDER(self));
}

void smh_jse_builder_action(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBAction *action, gboolean full) {
    (void)json_builder_begin_object(JSON_BUILDER(self));
    (void)json_builder_set_member_name(JSON_BUILDER(self), "act_id");
    (void)json_builder_add_int_value(JSON_BUILDER(self), action->id);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "act_num");
    (void)json_builder_add_int_value(JSON_BUILDER(self), action->number);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "zcl_zcl_id");
    (void)json_builder_add_int_value(JSON_BUILDER(self), action->cluster);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "oppy_key");
    (void)json_builder_add_int_value(JSON_BUILDER(self), action->command);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "scrt_scrt_id");
    (void)json_builder_add_int_value(JSON_BUILDER(self), action->automation);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "mmtp_mmtp_id");
    (void)json_builder_add_int_value(JSON_BUILDER(self), action->moment);

    // Parameters

    (void)json_builder_set_member_name(JSON_BUILDER(self), "params");

    (void)json_builder_begin_array(JSON_BUILDER(self));

    for (gsize index = 0; index < action->n_parameters; index++) {
        gint32 parameter = action->parameters[index];
        json_builder_add_int_value(JSON_BUILDER(self), parameter);
    }

    (void)json_builder_end_array(JSON_BUILDER(self));

    // Devices

    (void)json_builder_set_member_name(JSON_BUILDER(self), "devices");

    (void)json_builder_begin_array(JSON_BUILDER(self));

    for (gsize index = 0; index < action->n_devices; index++) {
        guint32 device = action->devices[index];
        json_builder_add_int_value(JSON_BUILDER(self), device);
    }

    (void)json_builder_end_array(JSON_BUILDER(self));

    // Groups

    (void)json_builder_set_member_name(JSON_BUILDER(self), "groups");

    (void)json_builder_begin_array(JSON_BUILDER(self));

    for (gsize index = 0; index < action->n_groups; index++) {
        guint32 group = action->groups[index];
        json_builder_add_int_value(JSON_BUILDER(self), group);
    }

    (void)json_builder_end_array(JSON_BUILDER(self));

    (void)json_builder_end_object(JSON_BUILDER(self));
}

void smh_jse_builder_condition_sensors(SMHJSEBuilder *self, SMH__PBEDB *db, GArray *conditions, gboolean full) {
    (void)json_builder_begin_array(JSON_BUILDER(self));

    for (guint index = 0; index < conditions->len; index++) {
        SMH__PBEDBCondition *condition = g_array_index(conditions, SMH__PBEDBCondition *, index);
        smh_jse_builder_condition_sensor(self, db, condition, full);
    }

    (void)json_builder_end_array(JSON_BUILDER(self));
}

void smh_jse_builder_condition_sensor(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBCondition *condition, gboolean full) {
    (void)json_builder_begin_object(JSON_BUILDER(self));
    (void)json_builder_set_member_name(JSON_BUILDER(self), "cnsn_id");
    (void)json_builder_add_int_value(JSON_BUILDER(self), condition->id);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "dev_dev_id");
    (void)json_builder_add_int_value(JSON_BUILDER(self), condition->peripheral);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "scrt_scrt_id");
    (void)json_builder_add_int_value(JSON_BUILDER(self), condition->automation);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "cnsn_zcl_id");
    (void)json_builder_add_int_value(JSON_BUILDER(self), condition->cluster);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "cnsn_attr_key");
    (void)json_builder_add_int_value(JSON_BUILDER(self), condition->attribute);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "sensor_value");
    (void)json_builder_add_int_value(JSON_BUILDER(self), condition->value);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "cnsn_type");
    (void)json_builder_add_int_value(JSON_BUILDER(self), condition->relation);
    (void)json_builder_end_object(JSON_BUILDER(self));
}

void smh_jse_builder_used(SMHJSEBuilder *self, gboolean result) {
    (void)json_builder_begin_object(JSON_BUILDER(self));
    (void)json_builder_set_member_name(JSON_BUILDER(self), "result");
    (void)json_builder_add_boolean_value(JSON_BUILDER(self), result);
    (void)json_builder_end_object(JSON_BUILDER(self));
}

void smh_jse_builder_get_mode(SMHJSEBuilder *self, SMH__PBEDB *db, gboolean full) {
    (void)json_builder_begin_object(JSON_BUILDER(self));

    // Current

    (void)json_builder_set_member_name(JSON_BUILDER(self), "current_mode");

    (void)json_builder_begin_array(JSON_BUILDER(self));

    g_autoptr(GArray) current_modes = smh__pbedb__ref_automations(db);
    GE_ARRAY_FILTER(current_modes, i, e, SMH__PBEDBAutomation *, (e->on == TRUE));
    for (guint index = 0; index < current_modes->len; index++) {
        SMH__PBEDBAutomation *mode = g_array_index(current_modes, SMH__PBEDBAutomation *, index);
        json_builder_add_int_value(JSON_BUILDER(self), mode->id);
    }

    (void)json_builder_end_array(JSON_BUILDER(self));

    // All

    (void)json_builder_set_member_name(JSON_BUILDER(self), "modes");
    g_autoptr(GArray) modes = smh__pbedb__ref_automations(db);
    GE_ARRAY_FILTER(modes, i, e, SMH__PBEDBAutomation *, (e->type == SMH__PBEDBAUTOMATION__TYPE__TYPE_MODE));
    smh_jse_builder_modes(self, db, modes, full);

    (void)json_builder_end_object(JSON_BUILDER(self));
}

void smh_jse_builder_modes(SMHJSEBuilder *self, SMH__PBEDB *db, GArray *modes, gboolean full) {
    (void)json_builder_begin_array(JSON_BUILDER(self));

    for (guint index = 0; index < modes->len; index++) {
        SMH__PBEDBAutomation *mode = g_array_index(modes, SMH__PBEDBAutomation *, index);
        smh_jse_builder_mode(self, db, mode, full);
    }

    (void)json_builder_end_array(JSON_BUILDER(self));
}

void smh_jse_builder_mode(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBAutomation *mode, gboolean full) {
    (void)json_builder_begin_object(JSON_BUILDER(self));
    (void)json_builder_set_member_name(JSON_BUILDER(self), "mode_id");
    (void)json_builder_add_int_value(JSON_BUILDER(self), mode->id);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "mode_name");
    (void)json_builder_add_string_value(JSON_BUILDER(self), mode->name);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "mode_type");
    (void)json_builder_add_int_value(JSON_BUILDER(self), mode->mode_type);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "mode_sign");
    (void)json_builder_add_int_value(JSON_BUILDER(self), mode->mode_sign);

    (void)json_builder_set_member_name(JSON_BUILDER(self), "schedulers");
    g_autoptr(GArray) schedulers = smh__pbedb__ref_schedulers(db);
    GE_ARRAY_FILTER(schedulers, i, e, SMH__PBEDBScheduler *, (e->automation == mode->id));
    smh_jse_builder_schedulers(self, db, schedulers, full);

    (void)json_builder_set_member_name(JSON_BUILDER(self), "activate_condition_sensors");
    g_autoptr(GArray) conditions_begin = smh__pbedb__ref_conditions(db);
    GE_ARRAY_FILTER(conditions_begin, i, e, SMH__PBEDBCondition *, ((e->automation == mode->id) && (e->moment == SMH__PBEDBACTION__MOMENT__MOMENT_BEGIN)));
    smh_jse_builder_condition_sensors(self, db, conditions_begin, full);

    (void)json_builder_set_member_name(JSON_BUILDER(self), "deactivate_condition_sensors");
    g_autoptr(GArray) conditions_end = smh__pbedb__ref_conditions(db);
    GE_ARRAY_FILTER(conditions_end, i, e, SMH__PBEDBCondition *, ((e->automation == mode->id) && (e->moment == SMH__PBEDBACTION__MOMENT__MOMENT_END)));
    smh_jse_builder_condition_sensors(self, db, conditions_end, full);

    (void)json_builder_set_member_name(JSON_BUILDER(self), "actions");
    g_autoptr(GArray) actions = smh__pbedb__ref_actions(db);
    GE_ARRAY_FILTER(actions, i, e, SMH__PBEDBAction *, (e->automation == mode->id));
    smh_jse_builder_actions(self, db, actions, full);

    (void)json_builder_end_object(JSON_BUILDER(self));
}

void smh_jse_builder_configure_buttons(SMHJSEBuilder *self, SMH__PBEDB *db, GArray *devices, gboolean full) {
    (void)json_builder_begin_array(JSON_BUILDER(self));

    for (guint index = 0; index < devices->len; index++) {
        SMH__PBEDBPeripheral *device = g_array_index(devices, SMH__PBEDBPeripheral *, index);
        smh_jse_builder_configure_button(self, db, device, full);
    }

    (void)json_builder_end_array(JSON_BUILDER(self));
}

void smh_jse_builder_configure_button(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBPeripheral *device, gboolean full) {
    (void)json_builder_begin_object(JSON_BUILDER(self));
    (void)json_builder_set_member_name(JSON_BUILDER(self), "dev_id");
    (void)json_builder_add_int_value(JSON_BUILDER(self), device->id);

    (void)json_builder_set_member_name(JSON_BUILDER(self), "bindings");

    (void)json_builder_begin_array(JSON_BUILDER(self));

    for (gsize index = 0; index < device->n_bindings; index++) {
        SMH__PBEDBBinding *binding = device->bindings[index];

        (void)json_builder_begin_object(JSON_BUILDER(self));
        (void)json_builder_set_member_name(JSON_BUILDER(self), "key_id");
        (void)json_builder_add_int_value(JSON_BUILDER(self), binding->key_id);
        (void)json_builder_set_member_name(JSON_BUILDER(self), "object_type");
        (void)json_builder_add_int_value(JSON_BUILDER(self), binding->object_type);
        (void)json_builder_set_member_name(JSON_BUILDER(self), "object_id");
        (void)json_builder_add_int_value(JSON_BUILDER(self), binding->object_id);
        (void)json_builder_set_member_name(JSON_BUILDER(self), "command_id");
        (void)json_builder_add_int_value(JSON_BUILDER(self), binding->command_id);
        (void)json_builder_end_object(JSON_BUILDER(self));
    }

    (void)json_builder_end_array(JSON_BUILDER(self));

    (void)json_builder_end_object(JSON_BUILDER(self));
}

void smh_jse_builder_mobile_registrations(SMHJSEBuilder *self, SMH__PBEDB *db, GArray *subscribers, gboolean full) {
    (void)json_builder_begin_array(JSON_BUILDER(self));

    for (guint index = 0; index < subscribers->len; index++) {
        SMH__PBEDBSubscriber *subscriber = g_array_index(subscribers, SMH__PBEDBSubscriber *, index);
        smh_jse_builder_mobile_registration(self, db, subscriber, full);
    }

    (void)json_builder_end_array(JSON_BUILDER(self));
}

void smh_jse_builder_mobile_registration(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBSubscriber *subscriber, gboolean full) {
    (void)json_builder_begin_object(JSON_BUILDER(self));
    (void)json_builder_set_member_name(JSON_BUILDER(self), "mobile_id");
    (void)json_builder_add_int_value(JSON_BUILDER(self), subscriber->id);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "registration_token");
    (void)json_builder_add_string_value(JSON_BUILDER(self), subscriber->registration_token);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "hw_id");
    (void)json_builder_add_string_value(JSON_BUILDER(self), subscriber->hw_id);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "mobile_name");
    (void)json_builder_add_string_value(JSON_BUILDER(self), subscriber->name);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "os");
    (void)json_builder_add_string_value(JSON_BUILDER(self), subscriber->os);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "STBdisconnect");
    (void)json_builder_add_int_value(JSON_BUILDER(self), subscriber->stb_disconnect);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "duration");
    (void)json_builder_add_int_value(JSON_BUILDER(self), subscriber->duration);
    (void)json_builder_end_object(JSON_BUILDER(self));
}

void smh_jse_builder_phone_numbers(SMHJSEBuilder *self, SMH__PBEDB *db, GArray *subscribers, gboolean full) {
    (void)json_builder_begin_array(JSON_BUILDER(self));

    for (guint index = 0; index < subscribers->len; index++) {
        SMH__PBEDBSubscriber *subscriber = g_array_index(subscribers, SMH__PBEDBSubscriber *, index);
        smh_jse_builder_phone_number(self, db, subscriber, full);
    }

    (void)json_builder_end_array(JSON_BUILDER(self));
}

void smh_jse_builder_phone_number(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBSubscriber *subscriber, gboolean full) {
    (void)json_builder_begin_object(JSON_BUILDER(self));
    (void)json_builder_end_object(JSON_BUILDER(self));
}

void smh_jse_builder_update_devices(SMHJSEBuilder *self, SMH__PBEDB *db, GArray *updates, gboolean full) {
    (void)json_builder_begin_array(JSON_BUILDER(self));

    for (guint index = 0; index < updates->len; index++) {
        SMH__PBEDBUpdate *update = g_array_index(updates, SMH__PBEDBUpdate *, index);
        smh_jse_builder_update_device(self, db, update, full);
    }

    (void)json_builder_end_array(JSON_BUILDER(self));
}

void smh_jse_builder_update_device(SMHJSEBuilder *self, SMH__PBEDB *db, SMH__PBEDBUpdate *update, gboolean full) {
    (void)json_builder_begin_object(JSON_BUILDER(self));
    (void)json_builder_end_object(JSON_BUILDER(self));
}

void smh_jse_builder_event(SMHJSEBuilder *self, SMH__PBEDBEvent *event) {
    (void)json_builder_begin_object(JSON_BUILDER(self));
    (void)json_builder_set_member_name(JSON_BUILDER(self), "type");
    (void)json_builder_add_int_value(JSON_BUILDER(self), event->type);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "object_id");
    (void)json_builder_add_int_value(JSON_BUILDER(self), event->object_id);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "zcl_id");
    (void)json_builder_add_int_value(JSON_BUILDER(self), event->cluster);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "attr_key");
    (void)json_builder_add_int_value(JSON_BUILDER(self), event->attribute);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "str_attr_value");
    (void)json_builder_add_string_value(JSON_BUILDER(self), event->str_value);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "num_attr_value");
    (void)json_builder_add_int_value(JSON_BUILDER(self), event->num_value);
    (void)json_builder_end_object(JSON_BUILDER(self));
}

void smh_jse_builder_default(SMHJSEBuilder *self) {
    (void)json_builder_begin_object(JSON_BUILDER(self));
    (void)json_builder_set_member_name(JSON_BUILDER(self), "code");
    (void)json_builder_add_int_value(JSON_BUILDER(self), 0);
    (void)json_builder_set_member_name(JSON_BUILDER(self), "message");
    (void)json_builder_add_string_value(JSON_BUILDER(self), _("Not supported in demo mode"));
    (void)json_builder_set_member_name(JSON_BUILDER(self), "source");
    (void)json_builder_add_string_value(JSON_BUILDER(self), "STB");
    (void)json_builder_end_object(JSON_BUILDER(self));
}
