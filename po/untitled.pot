# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-03-06 22:17+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../smhjsebuilder.c:661
msgid "Not supported in demo mode"
msgstr ""

#: ../smhpbedb.c:62
msgid "SMS notification service is not connected (error 317)"
msgstr ""

#: ../smhpbedb.c:94
msgid "Color lamp"
msgstr ""

#: ../smhpbedb.c:269
msgid "Color lamp-1"
msgstr ""

#: ../smhpbedb.c:444
msgid "Lamp"
msgstr ""

#: ../smhpbedb.c:607
msgid "Socket"
msgstr ""

#: ../smhpbedb.c:696
msgid "Socket-1"
msgstr ""

#: ../smhpbedb.c:785
msgid "Temperature sensor"
msgstr ""

#: ../smhpbedb.c:922
msgid "Keychain"
msgstr ""

#: ../smhpbedb.c:1011
msgid "Siren"
msgstr ""

#: ../smhpbedb.c:1117
msgid "Motion sensor"
msgstr ""

#: ../smhpbedb.c:1212
msgid "Door sensor"
msgstr ""

#: ../smhpbedb.c:1307
msgid "Leakage sensor"
msgstr ""

#: ../smhpbedb.c:1402
msgid "Smoke sensor"
msgstr ""

#: ../smhpbedb.c:1497
msgid "Gas sensor"
msgstr ""
