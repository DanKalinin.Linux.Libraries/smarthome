//
// Created by dan on 23.08.2019.
//

#ifndef LIBRARY_SMARTHOME_SMHJSEGENERATOR_H
#define LIBRARY_SMARTHOME_SMHJSEGENERATOR_H

#include "smhmain.h"

G_BEGIN_DECLS

#define SMH_TYPE_JSE_GENERATOR smh_jse_generator_get_type()

G_DECLARE_DERIVABLE_TYPE(SMHJSEGenerator, smh_jse_generator, SMH, JSE_GENERATOR, JSEGenerator)

struct _SMHJSEGeneratorClass {
    JSEGeneratorClass super;
};

G_END_DECLS

#endif //LIBRARY_SMARTHOME_SMHJSEGENERATOR_H
